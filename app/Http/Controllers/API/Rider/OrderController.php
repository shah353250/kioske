<?php

namespace App\Http\Controllers\API\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//models
use App\Models\Order;
use App\Models\OrderDetail;
//service
use App\Services\FirebaseNotificationService;

class OrderController extends Controller
{
    public function viewOrders(Request $request)
    {
        try {
            $orderArray = [];
            $orders = Order::with("orderDetails","customer")->where("status","processing")->where("rider_id",$request->user()->id)->latest()->get();
            foreach ($orders as $key => $value) {
                $singleOrderObject = new \stdClass;
                $singleOrderObject->order_id = $value->id;
                $singleOrderObject->status = $value->status;
                $singleOrderObject->username = $value->customer->name;
                $singleOrderObject->email = $value->customer->email;
                $singleOrderObject->phone = $value->customer->phone;
                $singleOrderObject->address = $value->delivery_address;
                $singleOrderObject->address_description = $value->address_description;
                $singleOrderObject->longitude = $value->longitude;
                $singleOrderObject->latitude = $value->latitude;
                $singleOrderObject->order_date = date('d-m-Y g:i a',strtotime($value->created_at));
                // $singleOrderObject->assign_date = ;
                $singleOrderObject->total_price = $value->total_price;
                
                foreach ($value->orderDetails as $childKey => $childValue) {
                    $singleOrderDetailObj = new \stdClass;
                    if($childValue->product_type == "bundles")
                    {
                        $singleOrderDetailObj->product_name = $childValue->bundle->name;
                        $singleOrderDetailObj->image_path =  $childValue->bundle->image_path;
                    }
                    else
                    {
                        $singleOrderDetailObj->product_name = $childValue->product->name;
                        $singleOrderDetailObj->image_path =  $childValue->product->image_path;
                    }
                    $singleOrderDetailObj->product_type = $childValue->product_type;
                    $singleOrderDetailObj->price = $childValue->price;
                    $singleOrderDetailObj->quantity = $childValue->quantity;
                    $singleOrderDetailObj->total_price = $childValue->total_price;
                    $singleOrderObject->order_details[$childKey] = $singleOrderDetailObj;
                }
                array_push($orderArray,$singleOrderObject);
            }
            return response()->json(["status"=>true,"data"=>$orderArray]);
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"error"=>$th->getMessage()]);
        }
    }

    public function updateOrderStatus(Request $request,FirebaseNotificationService $service)
    {
        try {
            $order = Order::where("id",$request->order_id)->where("status","processing")->first();
            if($order)
            {
                $order->status = strtolower($request->status);
                $order->save();
                if($request->status == "delivered"){
                    $title = "Your Order Delivered !!";
                }else{
                    $title = "Your Order Rejected !!";
                }
                $service->orderDeliveredNotify($order->customer->notification_token,$title,"View Order Details for more Info. Order # ".$order->order_reciept);
                return response()->json(["status"=>true,"message"=>"Order Status updated successfully"]);
            }else
            {
                return response()->json(["status"=>false,"error"=>"Invalid Order ID ".$request->order_id]);
            }
            
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"error"=>$th->getMessage()]);
        }
    }
    public function orderDetails($orderId)
    {
        $value = Order::with("orderDetails","customer")->find($orderId);
        if($value)
        {
                $singleOrderObject = new \stdClass;
                $singleOrderObject->order_id = $value->id;
                $singleOrderObject->status = $value->status;
                $singleOrderObject->username = $value->customer->name;
                $singleOrderObject->email = $value->customer->email;
                $singleOrderObject->phone = $value->customer->phone;
                $singleOrderObject->address = $value->delivery_address;
                $singleOrderObject->address_description = $value->address_description;
                $singleOrderObject->longitude = $value->longitude;
                $singleOrderObject->latitude = $value->latitude;
                $singleOrderObject->order_date =date('d-m-Y g:i a',strtotime($value->created_at));;
                $singleOrderObject->assign_date = date('d-m-Y g:i a',strtotime($value->assign_date));;
                $singleOrderObject->total_price = $value->total_price;
                
                foreach ($value->orderDetails as $childKey => $childValue) {
                    $singleOrderDetailObj = new \stdClass;
                    if($childValue->product_type == "bundles")
                    {
                        $singleOrderDetailObj->product_name = $childValue->bundle->name;
                        $singleOrderDetailObj->image_path =  $childValue->bundle->image_path;
                    }
                    else
                    {
                        $singleOrderDetailObj->product_name = $childValue->product->name;
                        $singleOrderDetailObj->image_path =  $childValue->product->image_path;
                    }
                    $singleOrderDetailObj->product_type = $childValue->product_type;
                    $singleOrderDetailObj->price = $childValue->price;
                    $singleOrderDetailObj->quantity = $childValue->quantity;
                    $singleOrderDetailObj->total_price = $childValue->total_price;
                    $singleOrderObject->order_details[$childKey] = $singleOrderDetailObj;
                }
            return response()->json(["status"=>true,"order" => $singleOrderObject]);
        }
        else{
            return response()->json(["status"=>false,"error" =>"Order Id is invalid"]);
        }
    }
}
