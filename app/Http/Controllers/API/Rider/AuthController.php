<?php

namespace App\Http\Controllers\API\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;

class AuthController extends Controller
{
    public function Login(Request $request)
    {
        $validation = Validator::make($request->only(['phone','password','notification_token']),[
            'phone'     => 'required|numeric|regex:/[0-9]{11}/',
            'password'  => 'required',
            'notification_token' => 'required'
        ]);
        if($validation->passes())
        {
            if(Auth::attempt(['phone' => $request->phone, 'password' => $request->password,"user_type"=>'rider']))
            {
                if(Auth::user()->active == 1)
                {
                    $userData = User::find(Auth::user()->id);
                    $userData->notification_token = $request->notification_token;
                    $userData->save();
                    $token = Auth::user()->createToken('Rider Token')->accessToken;
                    return response()->json([ 'status' => true, 'access_token' => $token, 'user_data' => $userData ]);
                }
                else{
                    Auth::logout();
                    return response()->json(['status'=>false,'error'=>['Your account is not active please contact to your admin.']]);
                }
            }
            else{
                return response()->json(['status'=>false,'error'=>['Invalid phone or password.']]);
            }
        }
        else{
            return response()->json(['status'=> false,'error'=>$validation->errors()->all()]);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([ 'status'=> true, 'message' => 'Successfully Logout!']);
    }
}
