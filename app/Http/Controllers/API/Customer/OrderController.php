<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Str;
//models
use App\User;
use App\Admin;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Bundle;
use App\Models\Product;
use App\Models\UserAddress;
//API resources
use App\Http\Resources\OldOrderResource;
use App\Http\Resources\BundleResource;
use App\Http\Resources\ProductResource;
use App\Mail\SendLoginDetailMail;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function placeOrder(Request $request)
    {
        DB::beginTransaction();
            try {
                if($request->address_id == -1)
                {
                    $userAddress = new  UserAddress;
                    $userAddress->address = $request->address;
                    $userAddress->longitude = $request->longitude;
                    $userAddress->latitude = $request->latitude;
                    $userAddress->description = $request->description;
                    $userAddress->address_type = $request->address_type;
                }
                else
                {
                    $userAddress =  UserAddress::find($request->address_id);
                }
                
                $order = Order::create([
                    "user_id" => $request->user()->id,
                    "total_price"=>0,
                    "status"=>'pending',
                    "delivery_address"=>$userAddress->address,
                    "longitude" => $userAddress->longitude,
                    "latitude" =>$userAddress->latitude,
                    "address_description" => $userAddress->description,
                ]);
                $totalPrice = 0;
                foreach($request->products as $item)
                {
                    if(strtolower($item["type"]) == "products")
                    {
                        $product = Product::findOrFail($item["id"]);
                        $totalPrice += $item["quantity"]*$product->discount_price;
                        OrderDetail::create([
                            "order_id" => $order->id,
                            "product_id" => $product->id,
                            "product_type" => "products",
                            "price" => $product->discount_price,
                            "quantity" => $item["quantity"],
                            "total_price" => $item["quantity"]*$product->discount_price
                        ]);
                    }
                    else if(strtolower($item["type"]) == "bundles")
                    {
                        $bundle = Bundle::findOrFail($item["id"]);
                        $bundlePrice =array_sum($bundle->bundleDetails->map(function($query){
                            return ($query->product->discount_price*$query->quantity);
                        })->toArray());
                        $bundlePrice = $bundlePrice-(($bundlePrice/100)*$bundle->discount_percent);
                        $totalPrice += $item["quantity"]*$bundlePrice;
                        OrderDetail::create([
                            "order_id" => $order->id,
                            "product_id" => $bundle->id,
                            "product_type" => "bundles",
                            "price" => $bundlePrice,
                            "quantity" => $item["quantity"],
                            "total_price" => $item["quantity"]*$bundlePrice
                        ]);
                    }
                }
                $order->update([
                    "total_price" => round($totalPrice)
                ]);
                DB::commit();
                Admin::all()->map(function($user) use ($order){
                    return $user->notify(new \App\Notifications\NewOrderNotification($order->id));
                });
                return response()->json(["status"=>true,"message"=>"Order Added Successfully"]);
            } catch (\Throwable $th) {
                DB::rollback();
                return response()->json(["status"=>false,"message"=>$th->getMessage()]);
            }
    }

    public function guestOrder(Request $request)
    {
        DB::beginTransaction();
        try {
                if(User::where("email", $request->email)->first() != null)
                {
                    return response()->json(["status"=>false,"error"=>"Email is already exist please login first or try with another email"]);
                }
                $generatedPassword = Str::random(8);
                //Add User First
                $userDetail = new User;
                $userDetail->name = $request->name;
                $userDetail->email = $request->email;
                $userDetail->phone = $request->phone;
                $userDetail->user_type = 'customer';
                $userDetail->password = $generatedPassword;
                $userDetail->notification_token = $request->notification_token;
                $userDetail->active = 1;
                $userDetail->save();
                //Order Address Details

                $userAddress = new  UserAddress;
                $userAddress->address = $request->address;
                $userAddress->longitude = $request->longitude;
                $userAddress->latitude = $request->latitude;
                $userAddress->description = $request->description;
                $userAddress->address_type = $request->address_type;

            $order = Order::create([
                "user_id" => $userDetail->id,
                "total_price"=>0,
                "status"=>'pending',
                "delivery_address"=>$userAddress->address,
                "longitude" => $userAddress->longitude,
                "latitude" =>$userAddress->latitude,
                "address_description" => $userAddress->description,
            ]);
            $totalPrice = 0;
            foreach($request->products as $item)
            {
                if(strtolower($item["type"]) == "products")
                {
                    $product = Product::findOrFail($item["id"]);
                    $totalPrice += $item["quantity"]*$product->discount_price;
                    OrderDetail::create([
                        "order_id" => $order->id,
                        "product_id" => $product->id,
                        "product_type" => "products",
                        "price" => $product->discount_price,
                        "quantity" => $item["quantity"],
                        "total_price" => $item["quantity"]*$product->discount_price
                    ]);
                }
                else if(strtolower($item["type"]) == "bundles")
                {
                    $bundle = Bundle::findOrFail($item["id"]);
                    $bundlePrice =array_sum($bundle->bundleDetails->map(function($query){
                        return ($query->product->discount_price*$query->quantity);
                    })->toArray());
                    $bundlePrice = $bundlePrice-(($bundlePrice/100)*$bundle->discount_percent);
                    $totalPrice += $item["quantity"]*$bundlePrice;
                    OrderDetail::create([
                        "order_id" => $order->id,
                        "product_id" => $bundle->id,
                        "product_type" => "bundles",
                        "price" => $bundlePrice,
                        "quantity" => $item["quantity"],
                        "total_price" => $item["quantity"]*$bundlePrice
                    ]);
                }
            }
            $order->update([
                "total_price" => round($totalPrice)
            ]);
            DB::commit();
            Admin::all()->map(function($user) use ($order){
                return $user->notify(new \App\Notifications\NewOrderNotification($order->id));
            });
            Mail::to($userDetail->email)->send(new SendLoginDetailMail(["email"=>$userDetail->email,"password"=>$generatedPassword]));
            $access_token = $userDetail->createToken('Api Token')->accessToken;
            return response()->json(["status"=>true,"message"=>"Order Added Successfully","access_token" => $access_token]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(["status"=>false,"message"=>$th->getMessage()]);
        }
    }

    public function userOrders(Request $request)
    {
        $orders = Order::with(["orderProducts","orderBundles","rider"])->where("user_id",$request->user()->id)->latest()->get();
        return OldOrderResource::collection($orders);
    }
    
    public function previousOrderProducts($order_id)
    {
        $products = OrderDetail::with("product")->where("order_id",$order_id)->where("product_type","products")->get();
        $bundles = OrderDetail::with("bundle")->where("order_id",$order_id)->where("product_type","bundles")->get();
        $data = [];
        foreach ($products as $key => $value) {

            $product = array(
                "id" => $value->product->id,
                "type" => "products",
                "name" => $value->product->name,
                "image" => $value->product->image_path,
                "price" => $value->product->price,
                "discount_price" => $value->product->discount_price,
                "quantity" => $value->quantity
            );
            $product = (object) $product;
            array_push($data,$product);
        }
        
        foreach ($bundles as $key => $value) {

            $bundlePrice =array_sum($value->bundle->bundleDetails->map(function($query){
                return ($query->product->discount_price*$query->quantity);
            })->toArray());
            $originalPrice = $bundlePrice;
            $bundlePrice = $bundlePrice-(($bundlePrice/100)*$value->bundle->discount_percent);
            $bundle = array(
                "id" => $value->bundle->id,
                "type" => "bundles",
                "name" => $value->bundle->name,
                "image" => $value->bundle->image_path,
                "price" => $originalPrice,
                "discount_price" => $bundlePrice,
                "quantity" => $value->quantity
            );
            $bundle = (object) $bundle;
            array_push($data,$bundle);
        }
        return response()->json(["data"=>$data]);
    }

}
