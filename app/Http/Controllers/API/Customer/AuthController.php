<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email:rfc|unique:users,email',
            'phone' => 'required',
            'password' => 'required',
            'address'   => 'required',
        ]);
        if ($validator->passes()) {
            $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => Hash::make($request->password),
                    'address'   => $request->address,
                    'user_type' => 'customer',
                    'active' => 1
            ]);
            $token = $user->createToken('Api Token')->accessToken;
            return response()->json(['status'=>true,'access_token'=>$token,"user_data"=>$user]);
        }
        else
        {
            return response()->json(['status'=> false,'error'=>$validator->errors()->all()]);
        }
    }

    public function login(Request $request)
    {
        try {
        $validator = Validator::make($request->only(['email','password','login_type','facebook_id','gmail_id']), [
            'login_type' => 'required|integer', //1 simple, 2 facebook, 3 gmail
        ]);
        
        $validator->sometimes('email', 'required|email:rfc', function ($input) {
            return $input->login_type == 1;
        });
        
        $validator->sometimes('password', 'required', function ($input) {
            return $input->login_type == 1;
        });
        
        $validator->sometimes('facebook_id', 'required', function ($input) {
            return $input->login_type == 2;
        });
        
        $validator->sometimes('gmail_id', 'required|unique:users,email', function ($input) {
            return $input->login_type == 3;
        });

        if ($validator->passes()) {
            
            if($request->login_type == 1) //simple
            {
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'active'=>1])) {
           
                    $userData = User::find(Auth::user()->id);
                    $userData->notification_token = $request->notification_token;
                    $userData->save();
                    $token =  Auth::user()->createToken('Api Token')->accessToken;
    
                    return response()->json(['status'=>true,'access_token'=>$token,'user_data' => Auth::user()]);
                }
                else
                {
                    return response()->json(['status'=>false,'error'=>['Invalid username or password.']]);
                }
            }
            else if($request->login_type == 2) //facebook
            {
                $user = User::firstOrCreate(
                    [
                        'facebook_id' => $request->facebook_id,
                    ],
                    [
                        'name' => @$request->name,
                        'user_type' => 'customer',
                        'active' => 1,
                        
                    ]
                );
                
                if (Auth::loginUsingId($user->id)) {
           
                    $token =  Auth::user()->createToken('Api Token')->accessToken;
                    $user->notification_token = $request->notification_token;
                    $user->save();
                    return response()->json(['status'=>true,'access_token'=>$token,'user_data' => Auth::user()]);
                }
                else
                {
                    return response()->json(['status'=>false,'error'=>['Invalid username or password.']]);
                }
            }
            else if($request->login_type == 3) //gmail
            {
                 $user = User::firstOrCreate(
                    [
                        'gmail_id' => $request->gmail_id,
                    ],
                    [
                        'name' => $request->name,
                        'email' => $request->gmail_id,
                        'gmail_id' => $request->gmail_id,
                        'user_type' => 'customer',
                        'active' => 1
                    ]
                );
                
                if (Auth::loginUsingId($user->id)) {
           
                    $user->notification_token = $request->notification_token;
                    $user->save();
                    $token =  Auth::user()->createToken('Api Token')->accessToken;
    
                    return response()->json(['status'=>true,'access_token'=>$token,'user_data' => $user]);
                }
                else
                {
                    return response()->json(['status'=>false,'error'=>['Invalid username or password.']]);
                }
            }
            else
            {
                return response()->json(['status'=>false,'error'=>['Invalid Login Type.']]);
            }
            
        }
        else
        {
            return response()->json(['status'=> false,'error'=>$validator->errors()->all()]);
        }
        } catch (\Throwable $th) {
            return response()->json(['status'=> false,'error'=>[$th->getMessage()]]);
        }
        
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([ 'status'=> true, 'message' => 'Successfully Logout!']);
    }
    public function forgotPassword(Request $request)
    {
        $user = User::where("email",$request->email)->whereNull('facebook_id')->whereNull('gmail_id')->first();
        if($user == null)
        {
            return response()->json(["status"=>false,"message"=>"This email is not Register."]);
        }
        $verifyCode = rand(1000,9999);
        Mail::to($user->email)->send(new ForgotPasswordMail($verifyCode));
        return response()->json(["status"=>true,"code"=>$verifyCode,"user_id"=>$user->id]);
    }
    public function changePassword(Request $request)
    {
        $user = User::find($request->user_id);
        if($user == null)
        {
            return response()->json(["status"=>false,"message"=>"User not found"]);
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(["status"=>true,"message"=>"Password changed successfully"]);
    }

    public function resetPassword(Request $request)
    {
        if($request->user()->facebook_id != null && $request->user()->gmail_id != null)
        {
            return response()->json(["status"=>false,"message"=>"not eligible to Reset Password"]);
        }
        if(Hash::check($request->old_password, $request->user()->password))
        {
            $user = User::find($request->user()->id);
            $user->password = Hash::make($request->new_password);
            $user->save();
            return response()->json(["status"=>true,"message"=>"Password changed successfully"]);
        }
        else
        {
            return response()->json(["status"=>false,"message"=>"Old Password didn't matched"]);
        }
    }
    public function editProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'email' => 'nullable|email:rfc|unique:users,email,'.Auth::user()->id,
        ]);
        if(!$validator->passes())
        {
            return response()->json(['status'=> false,'error'=>$validator->errors()->all()]);
        }
        $user =  Auth::user();
        if($request->has('name'))
            $user->name = $request->name;
        if($request->has('email'))
                $user->email = $request->email;
        if($request->has('phone'))
            $user->phone = $request->phone;
        $user->save();
        return response()->json(["status"=>true,"user_data"=>$user]);
        
    }
}
