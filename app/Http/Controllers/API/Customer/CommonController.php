<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Models\SubCategory;
use App\Models\Product;
use Validator;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\BundleResource;
use App\Models\Bundle;
use App\Models\UserAddress;
use Auth;
class CommonController extends Controller
{
    public function getCategories()
    {
        $porductcategories = ProductCategory::where('active',1)->get();
        // return response()->json(['status'=>true,'data'=>$porductcategories]);
        return response()->json(["status"=>true,"data"=>CategoryResource::collection($porductcategories)]);
    }
    public function getSubCategories($categoryId)
    {
        $validator = Validator::make(['categoryId'=>$categoryId], [
            'categoryId' => 'required|exists:product_categories,id', 
        ]);
        
        if ($validator->passes()) {
            $porductcategories = SubCategory::with('products')->where([['active',1],['product_category_id',$categoryId]])->get();
            return response()->json(['status'=>true,'data'=>SubCategoryResource::collection($porductcategories)]);
        }
        else
        {
            return response()->json(['status'=> false,'error'=>$validator->errors()->all()]);
        }
    }
    public function getProducts()
    {
        $porducts = Product::where('active',1)->get();
        return response()->json(['status'=>true,'data'=>ProductResource::collection($porducts)]);
    }
    public function getProduct($id)
    {
        $porduct = Product::findOrFail($id);
        return response()->json(['status'=>true,'data'=>new ProductResource($porduct)]);
    }
    public function getBundles()
    {
        $bundles = Bundle::with('bundleDetails')->where('active',1)->get();
        return response()->json(['status'=>true,'data'=>BundleResource::collection($bundles)]);
    }
    public function productSearch($search)
    {
        $products = Product::query()->where(function($query) use ($search){
            $query->whereHas('category', function($q) use ($search){
                $q->join('sub_category_translations', 'sub_categories.id', '=', 'sub_category_translations.sub_category_id')->where('sub_category_translations.subcategory', 'like', "%{$search}%");
            })->orWhereHas('translations',function ($querynew) use ($search) {
                $querynew->where('name', 'like',"%{$search}%");
            });
        })->where(function($q){
            $q->where('active',1);
        })->get();
        return response()->json(['status'=>true,'data'=>ProductResource::collection($products)]);
    }

    public function getAddresses()
    {
        $addresses = UserAddress::where('user_id',Auth::id())->get();
        return response()->json(["status"=>true,"data"=>$addresses]);
    }
    public function addAddress(Request $request)
    {
        // $exist = UserAddress::where('user_id',Auth::id())->where('address_type',$request->address_type)->exists();
        // if($exist)
        // {
        //     return response()->json(["status"=>false,"message"=>"Address Type already exist"]);
        // }
        // else
        // {
            if($request->has('id') && $request->id != 0)
            {
                $address = UserAddress::find($request->id);
            }
            else
            {
                $address =new UserAddress;
            }
            
            $address->address = $request->address;
            $address->address_type = $request->address_type;
            $address->longitude = $request->longitude;
            $address->latitude = $request->latitude;
            $address->description = $request->description;
            $address->user_id = Auth::id();
            $address->save();
            return response()->json(["status"=>true,"data"=>$address]);
        // }
    }
    public function updateAddress(Request $request)
    {
        $address = UserAddress::where('id',$request->id)->where('user_id',Auth::id())->first();
        if($address == null)
        {
            return response()->json(["status"=>false,"message"=>"Address Record Not Found"]);
        }
        $address->address = $request->address;
        // $address->address_type = $request->address_type;
        $address->longitude = $request->longitude;
        $address->latitude = $request->latitude;
        // $address->user_id = Auth::id();
        $address->save();
        return response()->json(["status"=>true,"data"=>$address]);
    }
    public function deleteAddress($id)
    {
        $address = UserAddress::where("id",$id)->where("user_id",Auth::user()->id)->first();
        if($address){
            $address->delete();
            return response()->json(["status" => true,"message"=>"Address deleted successfully"]);
        }
        else
        {
            return response()->json(["status" => false,"message"=>"Address record not found."]);
        }

    }
}
