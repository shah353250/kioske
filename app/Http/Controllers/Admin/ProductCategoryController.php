<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Models\SubCategory;
use Str;
use File;
use Illuminate\Validation\Rule;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        return view('admin.productcategory.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'en_name' => 'required|string|max:255|unique:product_category_translations,name',
            'ur_name' => 'required|string|max:255|unique:product_category_translations,name',
            'image' => 'required|image',
        ]);
        $imageName = Str::random(5).'_'.time().'.'.$request->image->extension();
        $article_data = [
            'en' => [
                'name'       => $request->en_name,
            ],
            'ur' => [
                'name'       => $request->ur_name,
            ],
            "image_name" => $imageName,
            "active" => $request->has('active')?1:0
         ];
         $request->image->move(public_path('/images/categories'), $imageName);
         ProductCategory::create($article_data);
         return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::findOrFail($id);
        return view('admin.productcategory.edit',compact('category')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'en_name' => 'required|string|max:255|unique:product_category_translations,name,'.$id.',product_category_id',
            'ur_name' => 'required|string|max:255|unique:product_category_translations,name,'.$id.',product_category_id',
            'image' => 'nullable|image',
            ]);
        $category = ProductCategory::findOrFail($id);
        if($request->hasFile('image'))
        {
            File::delete(public_path('/images/categories/'.$category->image_name));
            $imageName = Str::random(5).'_'.time().'.'.$request->image->extension();
            $request->image->move(public_path('/images/categories'), $imageName);
            $category->image_name = $imageName;
        }
        $category->translate('en')->name = $request->en_name;
        $category->translate('ur')->name = $request->ur_name;
        $category->active = $request->has('active')?1:0;
        if(!$request->has('active'))
        {
            if(SubCategory::where("product_category_id",$id)->where('active',1)->exists()){
                return redirect()->back()->withErrors(['active'=>'Can\'t deactive category because sub categories available.']);
            }
        }
        $category->save();
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
