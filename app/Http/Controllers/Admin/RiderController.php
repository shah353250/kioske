<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Validator;

class RiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $User = User::where('user_type','rider')->get();
        return view('admin.riders.index',compact('User'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.riders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'name'                => 'required',
            'email'               => 'nullable|unique:users',
            'phone'               => ['required','numeric','regex:/[0-9]{11}/',Rule::unique('users')->where(function ($query) {
                                        $query->where('user_type', 'rider');
                                    })],
            'nic'                 => 'required|unique:users',
            'address'             => 'required',
            'password'            => 'required_with:confirm_password|same:confirm_password|min:6',
            'confirm_password'    => 'min:6',

        ]);
        // ,[
        //     'name.required'        => 'This field is required',
        //     'email.required'       => 'This field is required',
        //     'phone.required'       => 'This field is required',
        //     'nic.required'         => 'This field is required',
        //     'address.required'     => 'This field is required',
        // ]
        

        $User               = new User;
        $User->name         = $request->name;
        $User->email        = $request->email;
        $User->password     = Hash::make($request->password);
        $User->phone        = $request->phone;
        $User->nic          = $request->nic;
        $User->address      = $request->address;
        $User->user_type    = 'rider';
        $User->active       = $request->has('is_active')?1:0;
        $User->save();

        return redirect()->route('riders.index')->withMessage('Rider Account Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User = User::find($id);
        return view('admin.riders.edit',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'                => 'required',
            'email'               => 'nullable|unique:users,email,'.$id,
            'phone'               => ['required','numeric','regex:/[0-9]{11}/',
                                        Rule::unique('users','phone')->where(function ($query) use ($id) {
                                        $query->where('user_type', 'rider')->where("id","!=",$id);
                                        })
                                    ],
            'nic'                 => 'required|unique:users,nic,'.$id,
            'address'             => 'required',

        ]);
        $validator->sometimes('password', 'required|required_with:confirm_password|same:confirm_password|min:6', function ($input) {
            return $input->password != null;
        });
        $validator->validate();
        $User               = User::find($id);
        $User->name         = $request->name;
        $User->email        = $request->email;
        if($request->has('password'))
        {
            $User->password     = Hash::make($request->password);
        }
        $User->phone        = $request->phone;
        $User->nic          = $request->nic;
        $User->address      = $request->address;
        $User->active       = $request->has('is_active')?1:0;
        $User->save();
        return redirect()->route('riders.index')->withMessage('Rider Account Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
