<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductUnit;
use App\Models\SubCategory;
use App\Models\Bundle;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Str;
use File;
use Image;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['category','unit'])->get();
        return view('admin.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_unit = ProductUnit::all();
        $product_category = SubCategory::where('active',1)->get();
        return view('admin.products.create',compact('product_unit','product_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name_en' =>'required',
            'product_code' => 'required|unique:products,product_code',
            'product_price' => 'required|numeric',
            'product_discount_price' => 'required|numeric|lte:product_price',
            'product_category' => 'required',
            'product_unit' => 'required',
            'thumbnail_image' => 'required|image|max:2048',
            'product_image' => 'required|array|min:1',
            'product_image.*' => 'required|image|max:2048',
            'product_description_en' => 'required',
            'product_description_ur' => 'required',
            'product_name_ur' =>'required',
            'product_cost_price' => 'required|numeric',
        ]);
        $imageName = Str::random(5).'_'.time().'.'.$request->thumbnail_image->extension();
        $product_data = [
            'en' => [
                'name'          => $request->product_name_en,
                'description'   => $request->product_description_en
            ],
            'ur' => [
                'name'          => $request->product_name_ur,
                'description'   => $request->product_description_ur
            ],
            "image_name"            => $imageName,
            "product_code"          => $request->product_code,
            'price'                 => $request->product_price,
            'discount_price'        => $request->product_discount_price,
            'cost_price'            => $request->product_cost_price,
            'sub_category_id'       => $request->product_category,
            'product_unit_id'       => $request->product_unit,
            "active"                => $request->has('active')?1:0,

         ];
         $product = Product::create($product_data);
        //  Image::make($request->thumbnail_image->getRealPath())->resize(600,600,function($constraint){
        //  })->save(public_path('/images/products').'/'.$imageName);
        move_uploaded_file($_FILES["thumbnail_image"]["tmp_name"], public_path('/images/products').'/'.$imageName);
        //  dd($request->product_image);
         foreach($request->product_image as $key =>$item)
         {
            $imageName = Str::random(5).'_'.time().'.'.$item->extension();
            Image::make($item->getRealPath())->resize(600,600,function($constraint){
            })->save(public_path('/images/products').'/'.$imageName);
            ProductImage::create(['product_id' => $product->id,'image'=>$imageName]);
         }

         return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $product_unit = ProductUnit::all();
        $product_category = SubCategory::where('active',1)->get();
        return view('admin.products.edit',compact('product_unit','product_category','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_name_en'           => 'required',
            'product_code'             => 'required|unique:products,product_code,'.$id,
            'product_price'             => 'required|numeric',
            'product_discount_price'    => 'required|numeric|lte:product_price',
            'product_category'          => 'required',
            'product_unit'              => 'required',
            'product_image'             => 'nullable|image|max:2048',
            'product_description_en'    => 'required',
            'product_description_ur'    => 'required',
            'product_name_ur'           => 'required',
            'product_cost_price'        => 'required|numeric',
        ]);
        $product = Product::findOrFail($id);
        $product->translate('en')->name             = $request->product_name_en;
        $product->translate('en')->description      = $request->product_description_en;
        $product->translate('ur')->name             = $request->product_name_ur;
        $product->translate('ur')->description      = $request->product_description_ur;

        if($request->hasFile('product_image'))
        {
            File::delete(public_path('/images/products/'.$product->product_image));
            $imageName = Str::random(5).'_'.time().'.'.$request->product_image->extension();
            $request->product_image->move(public_path('/images/products'), $imageName);
            $product->image_name = $imageName;
        }
        $product->price                 = $request->product_price;
        $product->product_code          = $request->product_code;
        $product->discount_price        = $request->product_discount_price;
        $product->cost_price            = $request->product_cost_price;
        $product->sub_category_id       = $request->product_category;
        $product->product_unit_id       = $request->product_unit;
        $product->active                = $request->has('active')?1:0;
        if(!$request->has('active'))
        {
            if(Bundle::query()->whereHas("bundleDetails",function($query) use ($id) {$query->where('product_id',$id );})->where('active',1)->exists()){
                return redirect()->back()->withErrors(['active'=>'Can\'t deactive product because this product used in bundles.']);
            }
        }
        $product->save();
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function importProducts(Request $request)
    {
        if($request->hasFile("excel_file"))
        {
            try {
                DB::beginTransaction();
                Excel::import(new ProductsImport, $request->excel_file);
                DB::commit();
                return response()->json(["status"=>true,"message"=>"Data Imported Successfully."]);
            } catch (\Throwable $th) {
                DB::rollBack();
                return response()->json(["status"=>false,"message"=>"Excel File is not well formed."]);
            }
        }
    }
}
