<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\User;
use App\Services\FirebaseNotificationService;
use DB;
class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::latest()->get();
        $riders = User::where('user_type','rider')->where("active",1)->get();
        return view("admin.orders.index",compact("orders","riders"));
    }
    public function detail($id,$notifyId = null)
    {
        if($notifyId != null)
        {
            DB::table("notifications")->where("id",$notifyId)->whereNull("read_at")->update(["read_at"=>NOW()]);
        }
        $order = Order::with(["orderProducts","orderBundles"])->findOrFail($id);
        return view("admin.orders.detail",compact("order"));
    }

    public function AjaxCallForAssignRider(Request $request,FirebaseNotificationService $service)
    {
        try {
            $order                  = Order::find($request->id);
            $order->rider_id        = $request->rider_id;
            $order->status          = 'processing';
            $order->is_assign       = 1;
            $order->assign_date     = NOW();
            $order->save();
            $rider = User::find($request->rider_id);
            $customer = User::find($order->user_id);
            $service->orderAssignNotifyRider($order,$rider->notification_token);
            $service->orderStatusCustomer($customer->notification_token);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
}
