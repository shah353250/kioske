<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProductUnit;

class ProductUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $ProductUnit = ProductUnit::all();
        $ProductUnit = ProductUnit::all();
        // dd($ProductUnit[1]->translate('en')->unit);
        return view('admin.productunit.index',compact('ProductUnit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productunit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'product_unit_en' => 'required|unique:product_unit_translations,unit',
            'product_unit_ur' => 'required|unique:product_unit_translations,unit',
        ],[
            'product_unit_en.required' => 'This field is required',
            'product_unit_ur.required' => 'This field is required',
        ]);

        $product_unit_data = [
            'en' => [
                'unit'       => $request->input('product_unit_en')
            ],
            'ur' => [
                'unit'       => $request->input('product_unit_ur')
            ],
         ];
     
         // Now just pass this array to regular Eloquent function and Voila!    
         ProductUnit::create($product_unit_data);
     
         // Redirect to the previous page successfully    
         return redirect()->route('productunit.index')->withMessage('Product Unit Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ProductUnit = ProductUnit::find($id);
        return view('admin.productunit.edit',compact('ProductUnit'));
        // dd($ProductUnit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_unit_en' => 'required|unique:product_unit_translations,unit,'.$id.',product_unit_id',
            'product_unit_ur' => 'required|unique:product_unit_translations,unit,'.$id.',product_unit_id',
        ],[
            'product_unit_en.required' => 'This field is required',
            'product_unit_ur.required' => 'This field is required',
        ]);

        $product_unit_data = [
            'en' => [
                'unit'       => $request->input('product_unit_en')
            ],
            'ur' => [
                'unit'       => $request->input('product_unit_ur')
            ],
        ];
     
        $ProductUnit = ProductUnit::findOrFail($id);
        $ProductUnit->update($product_unit_data);
    
        return redirect()->route('productunit.index')->withMessage('Product Unit Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
