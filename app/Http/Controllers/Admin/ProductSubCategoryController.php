<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Models\SubCategory;
use App\Models\Product;

class ProductSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProductSubCategory = SubCategory::all();
        return view('admin.productSubCategory.index',compact('ProductSubCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ProductCategory = ProductCategory::where('active',1)->get();
        return view('admin.productSubCategory.create',compact('ProductCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'product_category'  => 'required',
            'sub_category_en'   => 'required|string|max:255|unique:sub_category_translations,subcategory',
            'sub_category_ur'   => 'required|string|max:255|unique:sub_category_translations,subcategory',
        ],[
            'product_category.required' => 'This field is required.',
            'sub_category_en.required'  => 'This field is required.',
            'sub_category_ur.required'  => 'This field is required.'
        ]);

        $sub_category_data = [
            'en' => [
                'subcategory'       => $request->sub_category_en,
            ],
            'ur' => [
                'subcategory'       => $request->sub_category_ur,
            ],
            "product_category_id"   => $request->product_category,
            "active"                => $request->has('is_active')?1:0,
        ];
        SubCategory::create($sub_category_data);
        return redirect()->route('sub-category.index')->withMessage('Product Sub Category Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ProductSubCategory = SubCategory::find($id);
        $ProductCategory = ProductCategory::all();
        return view('admin.productSubCategory.edit',compact('ProductSubCategory','ProductCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_category'  => 'required',
            'sub_category_en'   => 'required|string|max:255|unique:sub_category_translations,subcategory,'.$id.',sub_category_id',
            'sub_category_ur'   => 'required|string|max:255|unique:sub_category_translations,subcategory,'.$id.',sub_category_id',
        ],[
            'product_category.required' => 'This field is required.',
            'sub_category_en.required'  => 'This field is required.',
            'sub_category_ur.required'  => 'This field is required.'
        ]);

        $SubCategory                                = SubCategory::findOrFail($id);
        $SubCategory->translate('en')->subcategory  = $request->sub_category_en;
        $SubCategory->translate('ur')->subcategory  = $request->sub_category_ur;
        $SubCategory->product_category_id           = $request->product_category;
        $SubCategory->active                        = $request->has('is_active')?1:0;
        if(!$request->has('active'))
        {
            if(Product::where("sub_category_id",$id)->where('active',1)->exists()){
                return redirect()->back()->withErrors(['active'=>'Can\'t deactive sub category because products available.']);
            }
        }
        $SubCategory->save();

        return redirect()->route('sub-category.index')->withMessage('Product Sub Category Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
