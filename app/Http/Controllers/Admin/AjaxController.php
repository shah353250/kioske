<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bundle;
use App\Models\BundleDetail;
use App\Models\Product;
use App\Models\ProductImage;
use Str;
use Image;
use Auth;
class AjaxController extends Controller
{
    public function AjaxCallForBundleProductDelete(Request $request)
    {
        try {
            BundleDetail::destroy($request->id);    
        } catch (\Throwable $th) {
            return false;
        }
        return true;
    }

    public function AjaxCallForViewBundle(Request $request)
    {
        $bundle = Bundle::find($request->id);
        return view('admin.bundles.modal',compact('bundle'));

        
    }

    public function RemoveProductImage($id)
    {
        $productImage = ProductImage::findOrFail($id);
        $productImage->delete();
        return response()->json(["status" => true,"message"=> ""]);
    } 
    public function AddProductImages(Request $request)
    {
        foreach($request->product_image as $key =>$item)
        {
        $imageName = Str::random(5).'_'.time().'.'.$item->extension();
        Image::make($item->getRealPath())->resize(600,600,function($constraint){
        })->save(public_path('/images/products').'/'.$imageName);
        ProductImage::create(['product_id' => $request->product_id,'image'=>$imageName]);
        }
        $view = view('admin.products._images',['images'=>ProductImage::where('product_id',$request->product_id)->get()]);
        return response()->json(["status"=> true,"data"=> $view->render()]);
    }

    public function AjaxCallForViewProduct(Request $request)
    {
        $Product = Product::find($request->id);
        return view('admin.products.productViewModal',compact('Product'));
    }

    public function notificationAlert()
    {
        $notifications = Auth::user()->unreadNotifications;
        $notificationsTemplate = view("admin.partials.notification",["notifications"=>$notifications])->render();
        return response()->json(["new_notification_count" =>count($notifications) ,"notifications"=>$notificationsTemplate]);
    }
}
