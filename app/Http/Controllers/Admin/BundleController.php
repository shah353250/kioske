<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bundle;
use App\Models\BundleDetail;
use App\Models\Product;
use DB;
use Str;
use File;
use Image;

class BundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function index()
    {
        $bundles = Bundle::all();
        // dd($bundles);
        return view('admin.bundles.index',compact('bundles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::query()->where("active",1)->get();
        return view('admin.bundles.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bundle_name_en' =>'required',
            'bundle_name_ur' =>'required',
            'bundle_description_en' => 'required',
            'bundle_description_ur' => 'required',
            'discount_percent' => 'required|numeric|min:0|max:100',
            'product.*' => 'required|distinct',
            'quantity.*' => 'required',
            'bundle_image' => 'required|image|max:2048',
        ],[
            'product.*.required' => 'The product field is required',
            'product.*.distinct' => 'The product is added to many time in table.',
            'quantity.*.required' => 'The quantity field is required',
        ]);
        $imageName = Str::random(5).'_'.time().'.'.$request->bundle_image->extension();

        DB::transaction(function() use ($request,$imageName){
            $bundle_data = [
                'en' => [
                    'name'       => $request->bundle_name_en,
                    'description' => $request->bundle_description_en
                ],
                'ur' => [
                    'name'       => $request->bundle_name_ur,
                    'description' => $request->bundle_description_ur,
                ],
                "image_name"            => $imageName,
                'discount_percent' => $request->discount_percent,
                "active" => $request->has('active')?1:0,
            ];
            $bundle = Bundle::create($bundle_data);
            foreach($request->product as $key => $product_id)
            {
                $bundleDetail = new BundleDetail;
                $bundleDetail->product_id = $product_id;
                $bundleDetail->bundle_id = $bundle->id;
                $bundleDetail->quantity =  $request->quantity[$key];
                $bundleDetail->save();
            }
            Image::make($request->bundle_image->getRealPath())->resize(600,600,function($constraint){
            })->save(public_path('/images/bundles').'/'.$imageName);
        });
        return redirect()->route('bundles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bundles = Bundle::find($id);
        $products = Product::query()->where("active",1)->get();
        return view('admin.bundles.edit',compact('bundles','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'bundle_name_en'            =>'required',
            'bundle_name_ur'            =>'required',
            'bundle_description_en'     => 'required',
            'bundle_description_ur'     => 'required',
            'discount_percent'          => 'required|numeric|min:0|max:100',
            'product.*'                 => 'required|distinct',
            'quantity.*'                => 'required',
            'bundle_image'              => 'nullable|image|max:2048',

        ],[
            'product.*.required'        => 'The product field is required',
            'product.*.distinct'        => 'The product is added to many time in table.',
            'quantity.*.required'       => 'The quantity field is required',
        ]);


        DB::transaction(function() use ($request,$id){
            $bundle = Bundle::findOrFail($id);
            $bundle->translate('en')->name          = $request->bundle_name_en;
            $bundle->translate('en')->description   = $request->bundle_description_en;
            $bundle->translate('ur')->name          = $request->bundle_name_ur;
            $bundle->translate('ur')->description   = $request->bundle_description_ur;
            $bundle->discount_percent               = $request->discount_percent;
            $bundle->active                         = $request->has('active')?1:0;

            if($request->hasFile('bundle_image'))
            {
                File::delete(public_path('/images/bundles/'.$bundle->image_name));
                $imageName = Str::random(5).'_'.time().'.'.$request->bundle_image->extension();
                $request->bundle_image->move(public_path('/images/bundles'), $imageName);
                $bundle->image_name = $imageName;
            }

            $bundle->save();

            foreach($request->product as $key => $product_id)
            {
                $bundleDetail               = isset($request->bundle_detail_id[$key])? BundleDetail::find($request->bundle_detail_id[$key]):new BundleDetail;
                $bundleDetail->product_id   = $product_id;
                $bundleDetail->bundle_id    = $bundle->id;
                $bundleDetail->quantity     =  $request->quantity[$key];
                $bundleDetail->save();
            }
        });
        return redirect()->route('bundles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
