<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\User;
use App\Models\Product;
class HomeController extends Controller
{
    public function index()
    {
        $ordersNo = Order::count();
        $customerNo = User::where('user_type','customer')->count();
        $ridersNo = User::where('user_type','rider')->count();
        $productsNo = Product::count();
        return view('admin.home.index',compact('ordersNo','customerNo','ridersNo','productsNo'));
    }
}
