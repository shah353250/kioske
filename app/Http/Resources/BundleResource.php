<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BundleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'discount_percentage' => $this->discount_percent,
            'image_path' => $this->image_path,
            'details' => $this->bundleDetails->map(function($item){
                $product = $item->product;
                $product->quantity = $item->quantity;
                return new ProductResource($product);
            }),
        ];
    }
}
