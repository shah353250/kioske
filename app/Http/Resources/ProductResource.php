<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'discount_price' => $this->discount_price,
            'unit' => $this->unit->unit,
            'image_path' => $this->image_path,
            'quantity' => isset($this->quantity)?$this->quantity:0,
            'images' => $this->productImages->map(function($item){
                return $item->image_path;
            }),
        ];
    }
}
