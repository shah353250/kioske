<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OldOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "order_id" => $this->id,
            "status" => $this->status,
            "order_date" => date('g:i a, F j, Y',strtotime($this->created_at)),
            "total_price" => $this->total_price,
            "rider_name" => $this->when($this->status == "processing", @$this->rider->name),
            "rider_number" => $this->when($this->status == "processing", @$this->rider->phone),
            "assign_date" => $this->when($this->status == "processing", date('d-m-Y g:i a',strtotime($this->assign_date))),
            "products_total" => $this->orderProducts->count(),
            "bundles_total"=>$this->orderBundles->count()
        ];
    }
}