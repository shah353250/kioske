<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Bundle extends Model implements TranslatableContract
{
    use Translatable;
    protected $fillable = ['discount_percent','active','image_name'];
    public $translatedAttributes = ['name','description'];

    public function getImagePathAttribute()
    {
        return asset("/images/bundles/{$this->image_name}");
    }

    public function bundleDetails()
    {
        return $this->hasMany('App\Models\BundleDetail','bundle_id');
    }
    public function orderDetails()
    {
        return $this->hasMany('App\Models\OrderDetail','product_id')->where('product_type',"bundles");
    }

}
