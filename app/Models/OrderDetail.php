<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{

    protected $fillable = ["order_id","product_id","product_type","price","quantity","total_price"];

    public function order()
    {
        return $this->belongsTo('App\Models\Order','order_id');
    }
    public function bundle()
    {
        return $this->belongsTo('App\Models\Bundle','product_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
