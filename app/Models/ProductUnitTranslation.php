<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUnitTranslation extends Model
{
    protected $fillable = ['unit'];
    public $timestamps = false;
}
