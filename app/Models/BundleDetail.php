<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BundleDetail extends Model
{
    protected $fillable = ['bundle_id','product_id','quantity'];
    
    public function bundle()
    {
        return $this->belongsTo('App\Models\Bundle','bundle_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
