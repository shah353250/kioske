<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class ProductUnit extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['unit'];

    public function products()
    {
        return $this->hasMany('App\Models\Product','product_unit_id');
    }
}
