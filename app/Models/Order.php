<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["user_id","total_price","status","delivery_address","longitude","latitude","address_description"];
    public function orderDetails()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id');
    }
    public function orderProducts()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id')->where('product_type', "products");
    }
    public function orderBundles()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id')->where('product_type', "bundles");
    }
    public function customer()
    {
        return $this->belongsTo("App\User","user_id")->where("user_type","customer");
    }
    public function rider()
    {
        return $this->belongsTo("App\User","rider_id")->where("user_type","rider");
    }
    public function getOrderRecieptAttribute()
    {
        return "Order-".$this->id;
    }
    public function getAssignDateAttribute($date)
    {
        return \Carbon\Carbon::parse($date);
    }
}
