<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model implements TranslatableContract
{
    use Translatable;
    protected $fillable = ['image_name','product_code','price','discount_price','sub_category_id','product_unit_id','active','cost_price'];
    public $translatedAttributes = ['name','description'];

    public function getImagePathAttribute()
    {
        return asset("/images/products/{$this->image_name}");
    }

    public function category()
    {
        return $this->belongsTo('App\Models\SubCategory','sub_category_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\ProductUnit','product_unit_id');
    }

    public function bundleDetails()
    {
        return $this->hasMany('App\Models\BundleDetail','product_id');
    }
    
    public function productImages()
    {
        return $this->hasMany('App\Models\ProductImage','product_id');
    }
    public function orderDetails()
    {
        return $this->hasMany('App\Models\OrderDetail','product_id')->where('product_type',"products");
    }
}
