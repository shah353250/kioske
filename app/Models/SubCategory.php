<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class SubCategory extends Model implements TranslatableContract
{
    use Translatable;
    protected $fillable = ['active','product_category_id'];
    public $translatedAttributes = ['subcategory'];

    public function productCategory()
    {
        return $this->belongsTo('App\Models\ProductCategory','product_category_id','id');
    }
    public function products()
    {
        return $this->hasMany('App\Models\Product','sub_category_id');
    }
}
