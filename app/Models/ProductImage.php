<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['product_id','image'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
    public function getImagePathAttribute()
    {
        return asset("/images/products/{$this->image}");
    }
}
