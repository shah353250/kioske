<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class ProductCategory extends Model implements TranslatableContract
{
    use Translatable;
    protected $fillable = ['image_name','active'];
    public $translatedAttributes = ['name'];

    public function getImagePathAttribute()
    {
        return asset("/images/categories/{$this->image_name}");
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','product_category_id');
    }
}
