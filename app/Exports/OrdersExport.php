<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OrdersExport implements FromView
{

    protected $orders;

    public function __construct()
    {
        $this->orders = Order::with(["orderDetails","customer"])->get();
    }

    public function view(): View
    {
        return view('admin.exports.orders_export', [
            'orders' => $this->orders
        ]);
    }
}
