<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use App\Models\ProductUnit;
use Illuminate\Contracts\View\View;

class ProductUnitExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('admin.exports.product_unit_export', [
            'productunit' => ProductUnit::all()
        ]);
    }
}
