<?php

namespace App\Imports;

use App\Models\Product;
// use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductsImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function model(array $row)
    // {
    //     return new Product([
    //         'en' => [
    //             'name'          => $row["name_english"],
    //             'description'   => $row["description_english"]
    //         ],
    //         'ur' => [
    //             'name'          => $row["name_urdu"],
    //             'description'   => $row["description_urdu"],
    //         ],
    //         "image_name"        => "no-product-image.jpg",
    //         "product_code"      => $row["product_code"],
    //         'price'             => $row["price"],
    //         'discount_price'    => $row["discount_price"],
    //         'cost_price'        => $row["cost_price"],
    //         'sub_category_id'   => $row["sub_category"],
    //         'product_unit_id'   => $row["product_unit"],
    //         "active"            => 1
    //      ]);
    // }
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            
            if(Product::where("product_code", trim($row["product_code"]))->exists())
            {
                $product = Product::where("product_code", trim($row["product_code"]))->first();
                $product->translate('en')->name             = $row["name_english"];
                $product->translate('en')->description      = $row["description_english"];
                $product->translate('ur')->name             = $row["name_urdu"];
                $product->translate('ur')->description      = $row["description_urdu"];
                $product->price                 = $row["price"];
                $product->discount_price        = $row["discount_price"];
                $product->cost_price            = $row["cost_price"];
                $product->sub_category_id       = $row["sub_category"];
                $product->product_unit_id       = $row["product_unit"];
                $product->active                = 1;
                $product->save();
            }
            else
            {
                $rowdata =[
                    'en' => [
                        'name'          => $row["name_english"],
                        'description'   => $row["description_english"]
                    ],
                    'ur' => [
                        'name'          => $row["name_urdu"],
                        'description'   => $row["description_urdu"],
                    ],
                    "image_name"        => "no-product-image.jpg",
                    "product_code"      => trim($row["product_code"]),
                    'price'             => $row["price"],
                    'discount_price'    => $row["discount_price"],
                    'cost_price'        => $row["cost_price"],
                    'sub_category_id'   => $row["sub_category"],
                    'product_unit_id'   => $row["product_unit"],
                    "active"            => 1
                ];
                Product::create($rowdata);
            }
            
        }
    }
}
