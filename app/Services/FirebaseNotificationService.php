<?php
namespace App\Services;

class FirebaseNotificationService
{
    public function orderAssignNotifyRider($order,$usertoken)
    {
        $notification = array(
            'title' => "New Order Assigned !!",
            'body' => "New order assign to you.",
            "image"=> "url-to-image"
        );
        $feilds = array('to'=>$usertoken, 'notification'=>$notification,"data"=>$order);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($feilds));
        $headers = array();
        $headers[] = 'Authorization: Key= '.env("FIREBASE_APP_KEY");
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    public function orderStatusCustomer($usertoken)
    {
        $notification = array(
            'title' => "Your Order Assigned !!",
            'body' => "View Order Details for more Info.",
            "image"=> "url-to-image"
        );
        $feilds = array('to'=>$usertoken, 'notification'=>$notification,"data"=>["type"=>"order"]); //order and offer type for notifications

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($feilds));
        $headers = array();
        $headers[] = 'Authorization: Key= '.env("FIREBASE_APP_KEY");
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }
    public function orderDeliveredNotify($usertoken,$title,$body)
    {
        $notification = array(
            'title' => $title,
            'body' => $body,
            "image"=> "url-to-image"
        );
        $feilds = array('to'=>$usertoken, 'notification'=>$notification,"data"=>["type"=>"order"]); //order and offer type for notifications

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($feilds));
        $headers = array();
        $headers[] = 'Authorization: Key= '.env("FIREBASE_APP_KEY");
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }
}