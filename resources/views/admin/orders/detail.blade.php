@extends('admin.layouts.app')
@push('custom-css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}">
@endpush
@section('content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row">
                                <div class="col-12">
                                    <h3>Customer Detail <p class="badge badge-secondary">{{ $order->order_reciept }}</p> <span class="float-right badge badge-info"><h6 class="m-0">Status: {{$order->status}}</h6></span></h3>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body row">
                            
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Name:</h5>
                                <p>{{$order->customer->name}} {{$order->customer->email == null ?"":"(".$order->customer->email.")"}}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Phone:</h5>
                                <p>{{$order->customer->phone??"N/A"}}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Delivery Address:</h5>
                                <p>{{$order->delivery_address??"N/A"}}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Address Detail:</h5>
                                <p>{{$order->address_description??"N/A"}}</p>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row">
                                <div class="col-12">
                                    <h3>Order</h3>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body row">
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Order Date:</h5>
                                <p>{{ date('d-m-Y H:i:s',strtotime($order->created_at)) }}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Total Price:</h5>
                                <p>{{$order->total_price}}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Total Bundles:</h5>
                                <p>{{$order->orderBundles->count()}}</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <h5>Total Products:</h5>
                                <p>{{$order->orderProducts->count()}}</p>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    @if ($order->status != "pending")
                        <div class="card mt-3">
                            <div class="card-header">
                                {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                                <div class="row">
                                    <div class="col-12">
                                        <h3>Rider Detail</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body row">
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <h5>Rider Name:</h5>
                                    <p>{{ $order->rider->name }}</p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <h5>Rider Phone:</h5>
                                    <p>{{$order->rider->phone}}</p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <h5>Assign Date:</h5>
                                    <p>{{$order->assign_date->diffForHumans()}}</p>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <h5>Order Status:</h5>
                                    @if ($order->status == "pending")
                                    <span class="badge badge-warning">Pending</span>
                                    @elseif ($order->status == "processing")
                                    <span class="badge badge-info">Processing</span>
                                    @elseif ($order->status == "delivered")
                                    <span class="badge badge-success">Delivered</span>
                                    @elseif ($order->status == "cancelled")
                                    <span class="badge badge-danger">Cancelled</span>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    @endif
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row">
                                <div class="col-12">
                                    <h3>Order Details</h3>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                      <tr>
                                        <th>Product/Bundle Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @foreach ($order->orderBundles as $item)
                                          <tr>
                                            <td>{{$item->bundle->name}}</td>
                                            <td>{{$item->price}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{$item->total_price}}</td>
                                          </tr>
                                      @endforeach
                                      @foreach ($order->orderProducts as $item)
                                        <tr>
                                            <td>{{$item->product->name}}</td>
                                            <td>{{$item->price}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{$item->total_price}}</td>
                                        </tr>
                                      @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                          <td colspan="3" class="text-right"><b>Total Price:</b> </td>
                                          <td><b>{{$order->total_price}}</b></td>
                                      </tr>
                                  </tfoot>
                                </table>
                              </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('custom-script')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    
@endpush
