@extends('admin.layouts.app')


@push('custom-css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
@endpush

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                    <h3>View all Orders <a href="{{route('export.orders')}}"
                                            class="btn btn-info btn-sm float-right">Export</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class=" ">
                            <br>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#menu1">Pending</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu2">Processing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu3">Delivered</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu4">Cancelled</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane active"><br>

                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <div class="table-responsive">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>S. No#</th>
                                                        <th>Order No</th>
                                                        <th>Customer Name</th>
                                                        <th>Total Price</th>
                                                        <th>Address</th>
                                                        <th>Address Detail</th>
                                                        <th>Phone</th>
                                                        <th>Status</th>
                                                        <th>Order Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
    
                                                    @foreach ($orders as $key => $item)
                                                    @if ($item->status == "pending")
                                                    <tr>
                                                        <td>{{++$key}}</td>
                                                        <td>{{$item->order_reciept}}</td>
                                                        <td>{{$item->customer->name}}</td>
                                                        <td>{{$item->total_price}}</td>
                                                        <td>{{$item->delivery_address}}</td>
                                                        <td>{{$item->address_description??"N/A"}}</td>
                                                        <td>{{$item->customer->phone}}</td>
                                                        <td>
                                                            @if ($item->status == "pending")
                                                            <span class="badge badge-warning">Pending</span>
                                                            @elseif ($item->status == "processing")
                                                            <span class="badge badge-info">Processing</span>
                                                            @elseif ($item->status == "delivered")
                                                            <span class="badge badge-success">Delivered</span>
                                                            @elseif ($item->status == "cancelled")
                                                            <span class="badge badge-danger">Cancelled</span>
                                                            @endif
                                                        </td>
                                                        <td>{{$item->created_at->diffForHumans()}}</td>
                                                        <td>
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                <a title="order details" class="btn btn-sm btn-warning mt-1"
                                                                    href="{{route("orders.detail",$item->id)}}"><i
                                                                        class="fa fa-eye"></i></a>
                                                                @if ($item->is_assign != 1)
                                                                <button title="assign to rider" data-orderID="{{$item->id}}"
                                                                    class="btn btn-sm btn-primary rider_assign_btn mt-1"><i
                                                                        class="fa fa-user-plus"></i></button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <div class="table-responsive">
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Order No</th>
                                                    <th>Customer Name</th>
                                                    <th>Total Price</th>
                                                    <th>Address</th>
                                                    <th>Address Detail</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Order Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($orders as $key => $item)
                                                @if ($item->status == "processing")
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$item->order_reciept}}</td>
                                                    <td>{{$item->customer->name}}</td>
                                                    <td>{{$item->total_price}}</td>
                                                    <td>{{$item->delivery_address}}</td>
                                                    <td>{{$item->address_description??"N/A"}}</td>
                                                    <td>{{$item->customer->phone}}</td>
                                                    <td>
                                                        @if ($item->status == "pending")
                                                        <span class="badge badge-warning">Pending</span>
                                                        @elseif ($item->status == "processing")
                                                        <span class="badge badge-info">Processing</span>
                                                        @elseif ($item->status == "delivered")
                                                        <span class="badge badge-success">Delivered</span>
                                                        @elseif ($item->status == "cancelled")
                                                        <span class="badge badge-danger">Cancelled</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$item->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <a title="order details" class="btn btn-sm btn-warning mt-1"
                                                                href="{{route("orders.detail",$item->id)}}"><i
                                                                    class="fa fa-eye"></i></a>
                                                            @if ($item->is_assign != 1)
                                                            <button title="assign to rider" data-orderID="{{$item->id}}"
                                                                class="btn btn-sm btn-primary rider_assign_btn mt-1"><i
                                                                    class="fa fa-user-plus"></i></button>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu3" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <div class="table-responsive">
                                        <table id="example3" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Order No</th>
                                                    <th>Customer Name</th>
                                                    <th>Total Price</th>
                                                    <th>Address</th>
                                                    <th>Address Detail</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Order Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($orders as $key => $item)
                                                @if ($item->status == "delivered")
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$item->order_reciept}}</td>
                                                    <td>{{$item->customer->name}}</td>
                                                    <td>{{$item->total_price}}</td>
                                                    <td>{{$item->delivery_address}}</td>
                                                    <td>{{$item->address_description??"N/A"}}</td>
                                                    <td>{{$item->customer->phone}}</td>
                                                    <td>
                                                        @if ($item->status == "pending")
                                                        <span class="badge badge-warning">Pending</span>
                                                        @elseif ($item->status == "processing")
                                                        <span class="badge badge-info">Processing</span>
                                                        @elseif ($item->status == "delivered")
                                                        <span class="badge badge-success">Delivered</span>
                                                        @elseif ($item->status == "cancelled")
                                                        <span class="badge badge-danger">Cancelled</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$item->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <a title="order details" class="btn btn-sm btn-warning mt-1"
                                                                href="{{route("orders.detail",$item->id)}}"><i
                                                                    class="fa fa-eye"></i></a>
                                                            @if ($item->is_assign != 1)
                                                            <button title="assign to rider" data-orderID="{{$item->id}}"
                                                                class="btn btn-sm btn-primary rider_assign_btn mt-1"><i
                                                                    class="fa fa-user-plus"></i></button>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu4" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <div class="table-responsive">
                                        <table id="example4" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Order No</th>
                                                    <th>Customer Name</th>
                                                    <th>Total Price</th>
                                                    <th>Address</th>
                                                    <th>Address Detail</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Order Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($orders as $key => $item)
                                                @if ($item->status == "cancelled")
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$item->order_reciept}}</td>
                                                    <td>{{$item->customer->name}}</td>
                                                    <td>{{$item->total_price}}</td>
                                                    <td>{{$item->delivery_address}}</td>
                                                    <td>{{$item->address_description??"N/A"}}</td>
                                                    <td>{{$item->customer->phone}}</td>
                                                    <td>
                                                        @if ($item->status == "pending")
                                                        <span class="badge badge-warning">Pending</span>
                                                        @elseif ($item->status == "processing")
                                                        <span class="badge badge-info">Processing</span>
                                                        @elseif ($item->status == "delivered")
                                                        <span class="badge badge-success">Delivered</span>
                                                        @elseif ($item->status == "cancelled")
                                                        <span class="badge badge-danger">Cancelled</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$item->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <a title="order details" class="btn btn-sm btn-warning mt-1"
                                                                href="{{route("orders.detail",$item->id)}}"><i
                                                                    class="fa fa-eye"></i></a>
                                                            @if ($item->is_assign != 1)
                                                            <button title="assign to rider" data-orderID="{{$item->id}}"
                                                                class="btn btn-sm btn-primary rider_assign_btn mt-1"><i
                                                                    class="fa fa-user-plus"></i></button>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                    <h3>View all Orders <a href="{{route('export.orders')}}"
class="btn btn-info btn-sm float-right">Export</a></h3>
</div>
</div>
</div>
<!-- /.card-header -->
<div class="card-body">
    <div id="msg"></div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>S. No#</th>
                <th>Order No</th>
                <th>Customer Name</th>
                <th>Total Price</th>
                <th>Address</th>
                <th>Address Detail</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Order Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($orders as $key => $item)
            <tr>
                <td>{{++$key}}</td>
                <td>{{$item->order_reciept}}</td>
                <td>{{$item->customer->name}}</td>
                <td>{{$item->total_price}}</td>
                <td>{{$item->delivery_address}}</td>
                <td>{{$item->address_description??"N/A"}}</td>
                <td>{{$item->customer->phone}}</td>
                <td>
                    @if ($item->status == "pending")
                    <span class="badge badge-warning">Pending</span>
                    @elseif ($item->status == "processing")
                    <span class="badge badge-info">Processing</span>
                    @elseif ($item->status == "delivered")
                    <span class="badge badge-success">Delivered</span>
                    @elseif ($item->status == "cancelled")
                    <span class="badge badge-danger">Cancelled</span>
                    @endif
                </td>
                <td>{{$item->created_at->diffForHumans()}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a title="order details" class="btn btn-sm btn-warning mt-1"
                            href="{{route("orders.detail",$item->id)}}"><i class="fa fa-eye"></i></a>
                        @if ($item->is_assign != 1)
                        <button title="assign to rider" data-orderID="{{$item->id}}"
                            class="btn btn-sm btn-primary rider_assign_btn mt-1"><i
                                class="fa fa-user-plus"></i></button>
                        @endif
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
</div>
</div>
</div>
</section>
</div> --}}

<div class="modal fade" id="assign_rider" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert bg-danger text-light pb-0" id="changePassError" style="display: none">
                </div>
                <form action="" method="post" id="rider_assign_form">

                    <input type="hidden" name="order_id" value="" id="order_id">
                    <div class="form-group">
                        <label for="">Select Rider</label>
                        <select name="" id="rider_id" class="form-control">
                            <option selected disabled>Select Option</option>
                            @foreach ($riders as $rider)
                            <option value="{{$rider->id}}">{{$rider->name}}</option>
                            @endforeach
                        </select>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="btnsubmit" class="btn btn-primary" form="rider_assign_form">Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection



@push('custom-script')

<!-- DataTables  & Plugins -->
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $("#example2").DataTable();
        $("#example3").DataTable();
        $("#example4").DataTable();


    });

    $('.rider_assign_btn').click(function (e) {
        e.preventDefault();

        let orderID = $(this).data('orderid');
        $('#order_id').val(orderID);
        $("#assign_rider").modal('show');
    });

    $("#btnsubmit").click(function (e) {
        e.preventDefault();
        let order_id = $("#order_id").val();
        let rider_id = $("#rider_id").val();
        let msg = '';


        if (rider_id != null) {
            Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: `Ok`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('AjaxCallForAssignRider')}}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: order_id,
                            rider_id: rider_id,
                        },
                        success: function (response) {
                            if (response) {
                                window.location.reload();
                            } else {
                                msg = `<div class="col-12 mt-3">
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                Woops!, There is some connection problem please try again later.
                                            </div>
                                        </div>`;
                                $("#msg").html(msg);
                                $("#assign_rider").modal('hide');
                            }
                        }
                    });
                }
            })
        } else {
            alert('Please Select rider.');
        }
    });

</script>

@endpush
