@extends('admin.layouts.app')


@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Add Product Unit</h1>
            </div>
            <div class="col-sm-6">
              
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Enter Product Unit</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" method="POST" action="{{route('productunit.store')}}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Unit (English)</label>
                                    <input type="text" name="product_unit_en" class="form-control"
                                        id="exampleInputEmail1" placeholder="Enter Product Unit (English)" value="{{old('product_unit_en')}}">
                                        @error('product_unit_en')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Product Unit (اردو)</label>
                                    <input type="text" name="product_unit_ur" class="form-control"
                                        id="exampleInputPassword1" placeholder="Enter Product Unit (Urdu)" value="{{old('product_unit_ur')}}">
                                        @error('product_unit_ur')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>


                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('productunit.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                
            </div>
            
        </div>
    </section>
</div>
@endsection


