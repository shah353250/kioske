@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush
@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                    <h3>Product Categories 
										{{-- <a href="{{route('export.orders')}}" class="btn btn-info btn-sm float-right">Export</a> --}}
											<a href="{{route('category.create')}}" class="btn btn-primary mb-3 float-right">Add Product Category</a>
									</h3>
                                </div>
                            </div>
                        </div>
                        <div class=" ">
                            <br>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#menu1">Active</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu2">Deactive</a>
                                </li>
                                
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane active"><br>

                                    <div class="card-body">
                                        <table class="table table-bordered table-striped" id="data-table">
											<thead>
												<tr>
													<th>Sr.No</th>
													<th>Name (English)</th>
													<th>Name (اردو)</th>
													<th>Image</th>
													<th>Active</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@php
													$count = 0;
												@endphp
												@foreach ($categories as $key => $item)
												
												@if ($item->active == 1)
												<tr>
													<td>{{++$count}}</td>
													<td>{{$item->translate('en')->name}}</td>
													<td>{{$item->translate('ur')->name}}</td>
													<td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
													</td>
													<td>{{$item->active == 1?'active':'deactive'}}</td>
													<td><a href="{{route('category.edit',$item->id)}}"
															class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endif
												
												@endforeach
											</tbody>
										</table>
                                        
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade"><br>
                                    <div class="card-body">
										<table class="table table-bordered table-striped" id="data-table1">
											<thead>
												<tr>
													<th>Sr.No</th>
													<th>Name (English)</th>
													<th>Name (اردو)</th>
													<th>Image</th>
													<th>Active</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@php
													$count1 = 0;
												@endphp
												@foreach ($categories as $key => $item)
												@if ($item->active != 1)
												<tr>
													<td>{{++$count1}}</td>
													<td>{{$item->translate('en')->name}}</td>
													<td>{{$item->translate('ur')->name}}</td>
													<td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
													</td>
													<td>{{$item->active == 1?'active':'deactive'}}</td>
													<td><a href="{{route('category.edit',$item->id)}}"
															class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
												</tr>
												@endif
												
												@endforeach
											</tbody>
										</table>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Product Categories</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <a href="{{route('category.create')}}" class="btn btn-primary mb-3">Add Product Category</a>
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name (English)</th>
                                        <th>Name (اردو)</th>
                                        <th>Image</th>
                                        <th>Active</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$item->translate('en')->name}}</td>
                                        <td>{{$item->translate('ur')->name}}</td>
                                        <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
                                        </td>
                                        <td>{{$item->active == 1?'active':'deactive'}}</td>
                                        <td><a href="{{route('category.edit',$item->id)}}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div> --}}
@endsection
@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
        $("#data-table").DataTable();
        $("#data-table1").DataTable();

    });

</script>
@endpush
