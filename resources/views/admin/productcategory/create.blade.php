@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary mt-4">
                <div class="card-header">
                  <h3 class="card-title">Add Product Category</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="card-body">
                    <img src="" height="150" width="auto" alt="" id="categoryImage" srcset="">
                    <div class="form-group">
                      <label for="en_name">Category Name (English)</label>
                      <input type="text" name="en_name" class="form-control" value="{{old('en_name')}}" id="en_name">
                      @error('en_name')
                          <p class="text-danger text-sm">{{$message}}</p>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="ur_name">Category Name (اردو)</label>
                      <input type="text" name="ur_name" class="form-control" value="{{old('ur_name')}}" id="ur_name">
                      @error('ur_name')
                          <p class="text-danger text-sm">{{$message}}</p>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="image">Product Category Image</label>
                      <input type="file" class="form-control-file" name="image" id="image" accept="image/*">
                      @error('image')
                          <p class="text-danger text-sm">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" value="1" name="active" id="active" checked>
                        <label class="form-check-label" for="active">Is Active</label>
                      </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('category.index')}}" class="btn btn-default">Back</a>
                  </div>
                </form>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@push('custom-script')
    <script>
      var fileInput = document.querySelector("#image")
      var ImageElement = document.querySelector("#categoryImage")
      
      fileInput.addEventListener("change",function(){
        if (fileInput.files && fileInput.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            ImageElement.setAttribute("src", e.target.result);
          }
          reader.readAsDataURL(fileInput.files[0]); // convert to base64 string
        }
      })
    </script>
@endpush