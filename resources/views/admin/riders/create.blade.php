@extends('admin.layouts.app')

@section('content')

<div class="content-wrapper">
    
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mt-3">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Enter Rider Detail</h3>
                        </div>
                        <form id="quickForm" method="POST" action="{{route('riders.store')}}">
                            @csrf
                            <div class="card-body row">
                                <div class="form-group col-md-6">
                                    <label for="">Name</label>
                                    <input type="text" name="name" class="form-control"
                                        id="" placeholder="Name" value="{{old('name')}}">
                                        @error('name')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Email <small>(Optional)</small> </label>
                                    <input type="email" name="email" class="form-control"
                                        id="" placeholder="example@mail.com" value="{{old('email')}}">
                                        @error('email')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                

                                <div class="form-group col-md-6">
                                    <label for="">Phone</label>
                                    <input type="text" name="phone" class="form-control"
                                        id="" placeholder="0300xxxxxxx" value="{{old('phone')}}">
                                        @error('phone')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">N.I.C</label>
                                    <input type="text" name="nic" class="form-control"
                                        id="" placeholder="xxxxx-xxxxxxx-x" value="{{old('nic')}}">
                                        @error('nic')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Address</label>
                                    <input type="text" name="address" class="form-control"
                                        id="" placeholder="xyz area, Karachi." value="{{old('address')}}">
                                        @error('address')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Password</label>
                                    <input type="password" name="password" class="form-control"
                                        id="" placeholder="Password">
                                        @error('password')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Confirm Password</label>
                                    <input type="password" name="confirm_password" class="form-control"
                                        id="" placeholder="Confirm Password">
                                        @error('confirm_password')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="form-check ">
                                        {{-- <label for="">Address</label> --}}
                                        <input type="checkbox" name="is_active" class="form-check-input" value="{{old('is_active')}}" value="1" checked> Is Active
                                            {{-- @error('address')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror --}}
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('riders.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                
            </div>
            
        </div>
    </section>
</div>
@endsection