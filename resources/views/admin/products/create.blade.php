@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{ asset('backend/myplugin/fileuploader/css/jquery.fileuploader.css') }}">
<link rel="stylesheet" href="{{ asset('backend/myplugin/fileuploader/css/jquery.fileuploader-theme-dragdrop.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css" integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg==" crossorigin="anonymous" />
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary card-tabs mt-3">
                <div class="card-header">
                    <h3 class="card-title">Add Product</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_name_en">Product Name (English)</label>
                                            <input type="text" name="product_name_en" class="form-control @error('product_name_en') is-invalid @enderror" value="{{old('product_name_en')}}" id="product_name_en">
                                        </div>
                                        <div class="form-group">
                                            <label for="product_description_en">Product Description (English)</label>
                                            <textarea name="product_description_en"  class="form-control @error('product_description_en') is-invalid @enderror" rows="5">{{old('product_description_en')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_name_ur">Product Name (اردو)</label>
                                            <input type="text" name="product_name_ur" class="form-control @error('product_name_ur') is-invalid @enderror" value="{{old('product_name_ur')}}" id="product_name_ur">
                                        </div>
                                        <div class="form-group">
                                            <label for="product_description_ur">Product Description (اردو)</label>
                                            <textarea name="product_description_ur"  class="form-control @error('product_description_ur') is-invalid @enderror" rows="5">{{old('product_description_en')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_code">Product Code</label>
                                        <input type="number" name="product_code" class="form-control @error('product_code') is-invalid @enderror" value="{{old('product_code')}}" id="product_code">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_price">Product Price</label>
                                        <input type="number" name="product_price" class="form-control @error('product_price') is-invalid @enderror" value="{{old('product_price')}}" id="product_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_discount_price">Product Discount Price</label>
                                        <input type="number" name="product_discount_price" class="form-control @error('product_discount_price') is-invalid @enderror" value="{{old('product_discount_price')}}" id="product_discount_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_cost_price">Product Cost Price</label>
                                        <input type="number" name="product_cost_price" class="form-control @error('product_cost_price') is-invalid @enderror" value="{{old('product_cost_price')}}" id="product_cost_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_category">Product Category</label>
                                        <select name="product_category"  class="form-control @error('product_category') is-invalid @enderror">
                                            <option value>--Select Product Category--</option>
                                            @foreach ($product_category as $item)
                                                <option value="{{$item->id}}" {{old('product_category') == $item->id?'selected':''}}>{{$item->subcategory}} | {{$item->productCategory->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_unit">Product Unit</label>
                                        <select name="product_unit"  class="form-control @error('product_unit') is-invalid @enderror">
                                            <option value>--Select Product Unit--</option>
                                            @foreach ($product_unit as $item)
                                                <option value="{{$item->id}}" {{old('product_unit') == $item->id?'selected':''}}>{{$item->unit}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="thumbnail_image">Thumbnail Image</label>
                                        <input type="file" accept="image/*" class="form-control-file @error('thumbnail_image') is-invalid @enderror" name="thumbnail_image" id="thumbnail_image">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <img src="" height="80" width="auto" alt="" id="productthumbnail" srcset="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="product_image">Product Images</label>
                                        <input type="file" accept="image/*" multiple class="form-control-file @error('product_image') is-invalid @enderror" name="product_image[]" id="fileUploader">
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" value="1" name="active" id="active" checked>
                                            <label class="form-check-label" for="active">Is Active</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <a href="{{route('products.index')}}" class="btn btn-default">Back to View</a>
                                </div>
                </form>
                </div>
                <!-- /.card -->
            </div>
            
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@push('custom-script')
<!-- dropzonejs -->
<script src="{{ asset('backend/myplugin/fileuploader/js/jquery.fileuploader.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js" integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g==" crossorigin="anonymous"></script>
    <script>
        $('#fileUploader').fileuploader({
            changeInput: '<div class="fileuploader-input p-3">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="{{ asset("backend/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png") }}">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 6,
            addMore: false,
            extensions: ['jpg', 'jpeg', 'png'],
            onRemove: function(item) {
                $.post('', {
                    file: item.name,
                    data: {
                        image_file_id: "{{ @$data->id }}",
                        file: item.name,
                        image_post_file_id: item.data.image_file_id,
                        "_token": $('meta[name="csrf-token"]').attr('content')
                    }
                });
            },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
        var fileInput = document.querySelector("#thumbnail_image")
        var ImageElement = document.querySelector("#productthumbnail")
        
        fileInput.addEventListener("change",function(){
            if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                ImageElement.setAttribute("src", e.target.result);
            }
            reader.readAsDataURL(fileInput.files[0]); // convert to base64 string
            }
        })
        
    </script>
    @if ($errors->any())
    <script>
        errors = <?php echo json_encode($errors->all()); ?>;
        errors = errors.map((item)=>{
            return `<p class="m-0 text-danger">${item}</p>`
        })
        Swal.fire({title:"Validation Error",html: errors.join(' ')})
    </script>
    @endif
@endpush