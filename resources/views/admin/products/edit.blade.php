@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css" integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg==" crossorigin="anonymous" />
<style>
    .btnRemoveImage
    {
        height: 30px;
        width: 30px;
        background: black;
        color: azure;
        top: 10px;
        right: 20px;
        padding: 2px 5px 0px 9px;
        border-radius: 50%;
        border: 1px solid azure;
        box-sizing: border-box
    }
    .image-div:hover>img{
        transition: 0.5s;
        filter: blur(20px);
        filter: grayscale(1);
    }
</style>
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary card-tabs mt-3">
                <div class="card-header p-0 pt-1">
                  <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                    {{-- <li class="pt-2 px-3"><h3 class="card-title">Add Product</h3></li> --}}
                    <li class="nav-item">
                        <a class="nav-link active show" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="false">Edit Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-two-image-tab" data-toggle="pill" href="#custom-tabs-two-image" role="tab" aria-controls="custom-tabs-two-image" aria-selected="false">Edit Images</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                    
                  <div class="tab-content" id="custom-tabs-two-tabContent">
                        <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                            <form action="{{route('products.update',$product->id)}}" id="mainform" method="post" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_name_en">Product Name (English)</label>
                                            <input type="text" name="product_name_en" class="form-control @error('product_name_en') is-invalid @enderror" value="{{$product->translate('en')->name}}" id="product_name_en">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="product_description_en">Product Description (English)</label>
                                            <textarea name="product_description_en"  class="form-control @error('product_description_en') is-invalid @enderror" rows="5">{{$product->translate('en')->description}}</textarea>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_name_ur">Product Name (اردو)</label>
                                            <input type="text" name="product_name_ur" form="mainform" class="form-control @error('product_name_ur') is-invalid @enderror" value="{{$product->translate('ur')->name}}" id="product_name_ur">
                                        </div>
                                        <div class="form-group">
                                            <label for="product_description_ur">Product Description (اردو)</label>
                                            <textarea name="product_description_ur" form="mainform"  class="form-control @error('product_description_ur') is-invalid @enderror" rows="5">{{$product->translate('ur')->description}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="product_code">Product Code</label>
                                        <input type="number" name="product_code" class="form-control @error('product_code') is-invalid @enderror" value="@if ($errors->has('product_code')){{old('product_code')}}@else{{$product->product_code}}@endif" id="product_code">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_price">Product Price</label>
                                        <input type="number" name="product_price" class="form-control @error('product_price') is-invalid @enderror" value="{{$product->price}}" id="product_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_discount_price">Product Discount Price</label>
                                        <input type="number" name="product_discount_price" class="form-control @error('product_discount_price') is-invalid @enderror" value="{{$product->discount_price}}" id="product_discount_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_cost_price">Product Cost Price</label>
                                        <input type="number" name="product_cost_price" class="form-control @error('product_cost_price') is-invalid @enderror" value="{{$product->cost_price}}" id="product_cost_price">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_category">Product Category</label>
                                        <select name="product_category"  class="form-control @error('product_category') is-invalid @enderror">
                                            <option value>--Select Product Category--</option>
                                            @foreach ($product_category as $item)
                                                <option value="{{$item->id}}" {{$product->sub_category_id == $item->id?'selected':''}}>{{$item->subcategory}} | {{$item->productCategory->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_unit">Product Unit</label>
                                        <select name="product_unit"  class="form-control @error('product_unit') is-invalid @enderror">
                                            <option value>--Select Product Unit--</option>
                                            @foreach ($product_unit as $item)
                                                <option value="{{$item->id}}" {{$product->product_unit_id == $item->id?'selected':''}}>{{$item->unit}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="product_image">Product Thumbnail</label>
                                        <input type="file" accept="image/*" class="form-control-file @error('product_image') is-invalid @enderror" name="product_image" id="product_image">
                                    </div>
                                    <div class="col-md-6">
                                        <img src="{{$product->image_path}}" id="productThumbnail" height="100" width="auto" alt="" srcset="">
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" value="1" name="active" id="active" {{$product->active == 1?'checked':''}} >
                                            <label class="form-check-label" for="active">Is Active</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <a href="{{route('products.index')}}" class="btn btn-default">Back to View</a>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-two-image" role="tabpanel" aria-labelledby="custom-tabs-two-image-tab">
                            <div class="row" id="imagesList">
                                @foreach ($product->productImages as $item)
                                    <div class="col-md-6 col-lg-2 mt-3 position-relative image-div">
                                        <img src="{{asset($item->image_path)}}" class="img-fluid rounded" alt="" srcset="">
                                        <a role="button" class="position-absolute btnRemoveImage" data-id="{{$item->id}}"><i class="fa fa-times"></i></a>
                                    </div>
                                @endforeach
                            </div>
                            <form action="{{route('AddProductImages')}}" method="post" name="ImagesForm" enctype="multipart/form-data">
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                            <div class="row pt-3">
                                <div class="form-group col-md-6">
                                    <input type="file" accept="image/*" class="form-control-file @error('product_image') is-invalid @enderror" name="product_image[]" multiple id="product_images">
                                </div>
                                <div class="col-md-6">
                                    <a href="{{route('products.index')}}" class="btn btn-default float-right ml-2">Back to View</a>
                                </div>
                            </div>
                        </form>
                        </div>
                  </div>
                </div>
                <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@push('custom-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js" integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g==" crossorigin="anonymous"></script>
    <script>
        var fileInput = document.querySelector("#product_image")
        var ImageElement = document.querySelector("#productThumbnail")
        
        fileInput.addEventListener("change",function(){
            if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                ImageElement.setAttribute("src", e.target.result);
            }
            reader.readAsDataURL(fileInput.files[0]); // convert to base64 string
            }
        })
        $(document).on('click','.btnRemoveImage',function(){
            dataId = $(this).attr('data-id');
            currentButton = $(this);
            url = "/remove-product-image/"+dataId
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    method:'GET',
                    url:url,
                    dataType:'JSON',
                    success:function(res)
                    {
                        $(currentButton).parents('div.image-div').remove();
                        if(res.status)
                        {
                            Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                            )
                        }
                    }
                })
            }
            })
        });
        //add images request
        $('#product_images').on('change',function(e){
            currentButton = $(this);
            e.preventDefault();
            $.ajax({
                url: "{{route('AddProductImages')}}",
                type: "POST",
                data:  new FormData(document.forms.namedItem('ImagesForm')),
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'JSON',
                beforeSend : function()
                {
                    $(currentButton).prop('disabled',true);
                },
                success: function(res)
                {
                    if(res.status)
                    {
                        document.getElementById("product_images").value = "";
                        $('#imagesList').html(res.data);
                    }
                },
                error: function(e) 
                {

                },
                complete:function()
                {
                    $(currentButton).prop('disabled',false);
                } 
            });
        })

        $('#NextButton').click(function(e){
            e.preventDefault();
            $('#custom-tabs-two-profile-tab').tab('show');
        })
    
    @if ($errors->any())
        errors = <?php echo json_encode($errors->all()); ?>;
        errors = errors.map((item)=>{
            return `<p class="m-0 text-danger">${item}</p>`
        })
        Swal.fire({title:"Validation Error",html: errors.join(' ')})
    @endif

    </script>
@endpush