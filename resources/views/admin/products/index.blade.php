@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}">
@endpush
@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                    <h3>View all Products 
                                        {{-- <a href="{{route('export.orders')}}"
                                            class="btn btn-info btn-sm float-right">Export</a> --}}
                                        </h3>
                                </div>
                                <div class="col-sm-12">
                                <a href="{{route('products.create')}}" class="btn btn-success btn-sm mb-3">Add Product</a>
                                <a href="{{route('export.products')}}" class="btn btn-info float-right btn-sm">Export
                                    Products</a>
                                <button class="btn btn-primary float-right btn-sm mr-2" id="btnImportProduct">Import
                                    Products</button>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <br>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#menu1">Active</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu2">Deactive</a>
                                </li>
                                
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane active"><br>

                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="data-table">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Product Code</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Discount Price</th>
                                                    {{-- <th>Cost Price</th> --}}
                                                    <th>Sub Category</th>
                                                    <th>Unit</th>
                                                    <th>Image</th>
                                                    <th>Active</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $count1 = 0;
                                                @endphp
                                                @foreach ($products as $key => $item)
                                                @if ($item->active == 1)
                                                    <tr>
                                                        <td>{{++$key}}</td>
                                                        <td>{{$item->product_code}}</td>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{$item->price}}</td>
                                                        <td>{{$item->discount_price}}</td>
                                                        {{-- <td>{{$item->cost_price}}</td> --}}
                                                        <td>{{$item->category->subcategory}}</td>
                                                        <td>{{$item->unit->unit}}</td>
                                                        <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
                                                        </td>
                                                        <td>{{$item->active == 1?'active':'deactive'}}</td>
                                                        <td>
                                                            <button class="btn btn-warning btn-sm view-product"
                                                                data-id="{{$item->id}}"><i class="fa fa-eye"></i></button>
                
                                                            <a href="{{route('products.edit',$item->id)}}"
                                                                class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="data-table1">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Product Code</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Discount Price</th>
                                                    {{-- <th>Cost Price</th> --}}
                                                    <th>Sub Category</th>
                                                    <th>Unit</th>
                                                    <th>Image</th>
                                                    <th>Active</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $count1 = 0;
                                                @endphp
                                                @foreach ($products as $key => $item)
                                                @if ($item->active != 1)
                                                <tr>
                                                    <td>{{++$count1}}</td>
                                                    <td>{{$item->product_code}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->price}}</td>
                                                    <td>{{$item->discount_price}}</td>
                                                    {{-- <td>{{$item->cost_price}}</td> --}}
                                                    <td>{{$item->category->subcategory}}</td>
                                                    <td>{{$item->unit->unit}}</td>
                                                    <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
                                                    </td>
                                                    <td>{{$item->active == 1?'active':'deactive'}}</td>
                                                    <td>
                                                        <button class="btn btn-warning btn-sm view-product"
                                                            data-id="{{$item->id}}"><i class="fa fa-eye"></i></button>
            
                                                        <a href="{{route('products.edit',$item->id)}}"
                                                            class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                             
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-import">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Excel File</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
                <div class="alert" id="importAlertMessage"></div>
               <form action="{{route('import.products')}}" method="post" id="importProductForm">
                    <div class="form-group">
                        <label for="">Upload File</label>
                        <input type="file" accept="" class="form-control-file" name="excel_file">
                    </div>
               </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="sendImportFile">Import Products</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

    <script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>
<script>
	$(document).ready(function () {

        $('#btnImportProduct').on('click',function(){
            $('#modal-import').modal('show');
            document.getElementById("importProductForm").reset();
        });
		$('.view-product').click(function(){
                let _this   = $(this);
                let id      = _this.data('id');
                $.ajax({
                    url:"{{route('AjaxCallForViewProduct')}}",    
                    type: "POST",    
                    data: {
                        _token: "{{ csrf_token() }}", 
                        id: id
                    },
                    
                    success:function(result){
                        console.log('result',result);
                        if(result)
                        {
                            $('#modal-lg').modal('show');
                            $('#modal-body').html(result);
                        }
                    }
                    
                })
        });
        $('#sendImportFile').on('click',function(){
            $.ajax({
                url: "{{route('import.products')}}",
                type: "POST",
                data:  new FormData(document.querySelector('#importProductForm')),
                contentType: false,
                cache: false,
                processData:false,
                dataType:'JSON',
                beforeSend : function()
                {
                    $('#sendImportFile').prop('disabled',true);
                },
                success: function(data)
                {
                    
                    // alert(data.message);
                    if(data.status)
                    {
                        $('#modal-import').modal('hide');
                        window.location.reload();
                    }
                    else
                    {
                        $('#importAlertMessage').addClass('alert-danger').text(data.message);
                    }

                }, error:function(jhxr,status,err){
                    console.log(jhxr);
                },
                complete:function(){
                    $('#sendImportFile').prop('disabled',false);
                }   
            });
        });    
    });
    $(function () {
        $("#data-table").DataTable({
            "pageLength": 100
        });
        $("#data-table1").DataTable({
            "pageLength": 100
        });
    });

</script>
@endpush
