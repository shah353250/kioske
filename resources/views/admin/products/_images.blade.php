@foreach ($images as $item)
    <div class="col-md-6 col-lg-2 position-relative image-div mt-3">
        <img src="{{asset($item->image_path)}}" class="img-fluid rounded" alt="" srcset="">
        <a role="button" class="position-absolute btnRemoveImage" data-id="{{$item->id}}"><i class="fa fa-times"></i></a>
    </div>
@endforeach