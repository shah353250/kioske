<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label>Product Thumnail:</label><br>
            <img src="{{$Product->image_path}}" height="200" width="auto" alt="" srcset="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Category: </label> <span>{{$Product->category->productCategory->name}}</span>
        </div>
        <div class="form-group">
            <label>Sub Category: </label> <span>{{$Product->category->subcategory}}</span>
        </div>
        <div class="form-group">
            <label>Price: </label> <span>{{$Product->price}}</span>
        </div>
        <div class="form-group">
            <label>Discount Price: </label> <span>{{$Product->discount_price}}</span>
        </div>
        <div class="form-group">
            <label>Cost Price: </label> <span>{{$Product->cost_price}}</span>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label>Name (en):</label>
            <p>{{$Product->translate('en')->name}}</p>
            <label>Description (en):</label><br>
            <p>{{$Product->translate('en')->description}}</p>

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">

            <label>Name (ur):</label>
            <p>{{$Product->translate('ur')->name}}</p>
            <label>Description (ur):</label><br>
            <p>{{$Product->translate('ur')->description}}</p>
        </div>

    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">

    </div>
</div>
<h2 style="text-align: center">Product Image</h2>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @php
        $i=0;
        @endphp
        @foreach ($Product->productImages as $item)
        <div class="carousel-item {{ ($i++ == 0)? 'active':''}}">
            <img class="d-block w-100" style="height: 500px" src="{{asset($item->image_path)}}" alt="">
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
