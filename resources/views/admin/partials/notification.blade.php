@if(count($notifications) > 0)
@foreach ($notifications as $item)
<a href="{{$item->data["order_detail_link"]."/".$item->id}}" class="dropdown-item">
    <i class="fa fa-first-order" aria-hidden="true"></i> New Order Recieved
    <span class="float-right text-muted text-sm">Order #: {{$item->data["order_id"]}}</span>
  </a>
<div class="dropdown-divider"></div>
@endforeach
@else
    <p class="text-center p-3">No new order found.</p>
@endif

