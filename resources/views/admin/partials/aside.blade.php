<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">KIOSK</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('backend/dist/img/avatar5.png')}}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('home')}}" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>
        <!-- SidebarSearch Form -->
        {{-- <div class="form-inline">
     <div class="input-group" data-widget="sidebar-search">
       <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
       <div class="input-group-append">
         <button class="btn btn-sidebar">
           <i class="fas fa-search fa-fw"></i>
         </button>
       </div>
     </div>
   </div> --}}
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{route('orders.index')}}" class="nav-link">
                    {{-- <i class="nav-icon fas fa-tree"></i> --}}
                    <i class="nav-icon fas fa-box"></i>
                    <p>
                        Orders
                    </p>
                </a>
            </li>
                @if (Auth::user()->user_type === "admin")
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        {{-- <i class="nav-icon fas fa-chart-pie"></i> --}}
                        <i class="nav-icon fas fa-balance-scale"></i>
                        <p>
                            Product Unit
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('productunit.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Unit</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('productunit.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Unit</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        {{-- <i class="nav-icon fas fa-chart-pie"></i> --}}
                        <i class="nav-icon fas fa-code-branch"></i>
                        <p>
                            Product Category
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('category.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('category.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Category</p>
                            </a>
                        </li>
                        
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fas fa-box-open"></i>
                        <p>
                            Product Sub Category
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        
                        <li class="nav-item">
                            <a href="{{route('sub-category.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Sub Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('sub-category.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Sub Category</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fab fa-raspberry-pi"></i>
                        <p>
                            Products
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('products.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Product</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('products.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Products</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-box"></i>
                        <p>
                            Bundles
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('bundles.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Bundle</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('bundles.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Bundles</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        {{-- <i class="nav-icon fas fa-chart-pie"></i> --}}
                        <i class="nav-icon fas fa-motorcycle"></i>
                        <p>
                            Riders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('riders.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Rider</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('riders.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Riders</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('customers.index')}}" class="nav-link">
                        {{-- <i class="nav-icon fas fa-tree"></i> --}}
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Customers
                        </p>
                    </a>
                </li>
                @endif
                
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
