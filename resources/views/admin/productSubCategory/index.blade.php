@extends('admin.layouts.app')


@push('custom-css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}">

    
@endpush

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{-- <h3 class="card-title">DataTable with default features</h3> --}}
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                    <h3>View all Product Sub Category 
                                            <a href="{{route('sub-category.create')}}" class="btn btn-success" style="float:right" >Add Product Sub Category</a>
                                            <a href="{{route('export.product-subcategory')}}" class="btn btn-primary mr-2" style="float:right" >Export Sub Category</a>
                                        </h3>
                                </div>
                            </div>
                        </div>
                        <div class=" ">
                            
                            <br>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#menu1">Active</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu2">Deactive</a>
                                </li>
                                
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane active"><br>

                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="example1">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Product Category</th>
                                                    <th>Product Sub Category (English)</th>
                                                    <th>Product Sub Category (اردو)</th>
                                                    <th>Active</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @php
                                                $count = 0;
                                                @endphp
                                                @foreach ($ProductSubCategory as $item)
                                                @if ($item->active == 1)
                                                <tr>
                                                    <td>{{++$count}}</td>
                                                    <td>{{$item->productCategory->translate('en')->name}}</td>
                                                    <td>{{$item->translate('en')->subcategory}}</td>
                                                    <td>{{$item->translate('ur')->subcategory}}</td>
                                                    <td>{{($item->active==1)? 'Active':'Deactive'}}</td>
                                                    <td>
                                                        <a href="{{route('sub-category.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="example2">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Product Category</th>
                                                    <th>Product Sub Category (English)</th>
                                                    <th>Product Sub Category (اردو)</th>
                                                    <th>Active</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @php
                                                $count1 = 0;
                                                @endphp
                                                @foreach ($ProductSubCategory as $item)
                                                @if ($item->active != 1)
                                                <tr>
                                                    <td>{{++$count1}}</td>
                                                    <td>{{$item->productCategory->translate('en')->name}}</td>
                                                    <td>{{$item->translate('en')->subcategory}}</td>
                                                    <td>{{$item->translate('ur')->subcategory}}</td>
                                                    <td>{{($item->active==1)? 'Active':'Deactive'}}</td>
                                                    <td>
                                                        <a href="{{route('sub-category.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                             
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">

                @if (Session::has('message'))
                <div class="col-12 mt-3">
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{Session::get('message')}}
                    </div>
                </div>
                @endif

                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3>Product Sub Category</h3>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{route('sub-category.create')}}" class="btn btn-success" style="float:right" >Add Product Sub Category</a>
                                </div>
                                
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S. No#</th>
                                        <th>Product Category</th>
                                        <th>Product Sub Category (English)</th>
                                        <th>Product Sub Category (اردو)</th>
                                        <th>Active</th>
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1
                                    @endphp

                                    @foreach ($ProductSubCategory as $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$item->productCategory->translate('en')->name}}</td>
                                        <td>{{$item->translate('en')->subcategory}}</td>
                                        <td>{{$item->translate('ur')->subcategory}}</td>
                                        <td>{{($item->active==1)? 'Active':'Deactive'}}</td>
                                        <td>
                                            <a href="{{route('sub-category.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div> --}}
@endsection



@push('custom-script')

    <!-- DataTables  & Plugins -->
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>

    <script>
        var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
        });
        $(function () {
            // if('{{Session::has('message')}}')
            // {
            //     toastr.success('{{Session::has('message')}}');
            // }
            $("#example1").DataTable();
            $("#example2").DataTable();

        });
    </script>
    
@endpush
