@extends('admin.layouts.app')


@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Sub Category</h1>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Enter Sub Category</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" method="POST" action="{{route('sub-category.update',$ProductSubCategory->id)}}">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6 @error('product_category') is-invalid @enderror">
                                        <label for="exampleInputEmail1">Category</label>
                                        <select name="product_category" id="" class="form-control">
                                            <option selected disabled>Select Option</option>
                                            @foreach ($ProductCategory as $item)
                                            <option value="{{$item->id}}"
                                                {{(old('product_category')!=null)? (old('product_category')==$item->id? 'selected':''):($ProductSubCategory->product_category_id==$item->id? 'selected':'')}}>
                                                {{$item->translate('en')->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('product_category')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                    </div>
                                    <div class="form-group col-md-6 @error('sub_category_en') is-invalid @enderror">
                                        <label for="">Sub Category (English)</label>
                                        <input type="text" name="sub_category_en" class="form-control" placeholder="Enter Sub Category (English)" value="{{(old('sub_category_en')!=null)? old('sub_category_en'):$ProductSubCategory->translate('en')->subcategory }}">
                                        @error('sub_category_en')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 @error('sub_category_ur') is-invalid @enderror">
                                        <label for="">Sub Category (اردو)</label>
                                        <input type="text" name="sub_category_ur" class="form-control" placeholder="Enter Sub Category (اردو)" value="{{(old('sub_category_ur')!=null)? old('sub_category_ur'):$ProductSubCategory->translate('ur')->subcategory}}">
                                        @error('sub_category_ur')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="form-check ">
                                            <input type="checkbox" name="is_active" id="is_active" class="form-check-input" value="{{old('is_active')!=null? old('is_active'):1}}"  {{(old('is_active')!=null)? (old('is_active')==1? 'checked':''):(($ProductSubCategory->active==1)?'checked':'')}} > Is Active
                                        </div>
                                        @error('active')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                                <a href="{{route('sub-category.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>

            </div>

        </div>
    </section>
</div>
@endsection
