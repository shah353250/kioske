<table>
    <thead>
        <tr>
            <th>Name English</th>
            <th>Description English</th>
            <th>Name Urdu</th>
            <th>Description Urdu</th>
            <th>Product Code</th>
            <th>Price</th>
            <th>Discount Price</th>
            <th>Cost Price</th>
            <th>Sub Category</th>
            <th>Product Unit</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($products as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->description}}</td>
                <td>{{$item->translate('ur')->name}}</td>
                <td>{{$item->translate('ur')->description}}</td>
                <td>{{$item->product_code}}</td>
                <td>{{$item->price}}</td>
                <td>{{@$item->discount_price}}</td>
                <td>{{@$item->cost_price}}</td>
                <td>{{@$item->sub_category_id}}</td>
                <td>{{$item->product_unit_id}}</td>
            </tr>
        @endforeach
    </tbody>
</table>