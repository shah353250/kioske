<table>
    <thead>
        <tr>
            <th>Product Unit ID</th>
            <th>Name English</th>
            <th>Name Urdu</th>
            {{-- <th>Active</th> --}}
        </tr>
    </thead>
    <tbody>
        @foreach ($productunit as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->translate('en')->unit}}</td>
                <td>{{$item->translate('ur')->unit}}</td>
                {{-- <td>{{$item->active}}</td> --}}
            </tr>
        @endforeach
    </tbody>
</table>