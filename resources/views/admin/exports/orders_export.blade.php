<table>
    <thead>
        <tr>
            <th><b>Order Number</b></th>
            <th><b>Order Status</b></th>
            <th><b>Customer Name</b></th>
            <th><b>Phone</b></th>
            <th><b>Email</b></th>
            <th><b>Delivery Address</b></th>
            <th><b>Address Description</b></th>
            <th><b>Product Name</b></th>
            <th><b>Product Type</b></th>
            <th><b>Quantity</b></th>
            <th><b>Order Date</b></th>
            <th><b>Total Price</b></th>
        </tr> 
    </thead>
    <tbody>
        @foreach ($orders as $item)
            
                
                @foreach ($item->orderDetails as $childKey => $child)
                <tr style="background-color: #dce0e1">
                    @if ($childKey ==0)
                        <td style="text-align: center; vertical-align: middle" rowspan="{{$item->orderDetails->count()}}">{{$item->order_reciept}}</td>
                        <td style="text-align: center; vertical-align: middle" rowspan="{{$item->orderDetails->count()}}">{{$item->status}}</td>
                    @endif
                    <td>{{$item->customer->name}}</td>
                    <td>{{$item->customer->phone}}</td>
                    <td>{{$item->customer->email??"N/A"}}</td>
                    <td>{{$item->delivery_address}}</td>
                    <td>{{$item->address_description}}</td>
                    <td>
                        @if ($child->product_type == "products")
                        {{$child->product->name}}
                        @else
                        {{$child->bundle->name}}
                        @endif
                    </td>
                    <td>{{$child->product_type}}</td>
                    <td>{{$child->quantity}}</td>
                    <td>{{date("Y-m-d h:i:s",strtotime($child->created_at))}}</td>
                    @if ($childKey ==0)
                        <td style="text-align: center; vertical-align: middle" rowspan="{{$item->orderDetails->count()}}">{{$item->total_price}}</td>
                    @endif
                </tr> 
                @endforeach
        @endforeach
    </tbody>
</table>