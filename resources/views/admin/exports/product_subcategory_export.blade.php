<table>
    <thead>
        <tr>
            <th>Sub Category ID</th>
            <th>Name English</th>
            <th>Name Urdu</th>
            <th>Active</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($subcategory as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->translate('en')->subcategory}}</td>
                <td>{{$item->translate('ur')->subcategory}}</td>
                <td>{{$item->active}}</td>
            </tr>
        @endforeach
    </tbody>
</table>