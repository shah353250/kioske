@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css" integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg==" crossorigin="anonymous" />

<style>
    #bundleDetailTable tbody td:first-child,#bundleDetailTable tbody th:first-child{
        width: 40%;
    }
</style>
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary mt-4">
                <div class="card-header">
                  <h3 class="card-title">Edit Bundle</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('bundles.update',$bundles->id)}}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                  <div class="card-body">
                      <div class="row">
                        <div class="form-group col-md-6">
                            <label for="bundle_name_en">Name (English)</label>
                            <input type="text" name="bundle_name_en" class="form-control @error('bundle_name_en') is-invalid @enderror" value="{{old('bundle_name_en')!=null? old('bundle_name_en'):$bundles->translate('en')->name}}" id="bundle_name_en">
                            @error('bundle_name_en')
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bundle_name_ur">Name (اردو)</label>
                            <input type="text" name="bundle_name_ur" class="form-control @error('bundle_name_ur') is-invalid @enderror" value="{{old('bundle_name_ur')!=null? old('bundle_name_ur'):$bundles->translate('ur')->name}}" id="bundle_name_ur">
                            @error('bundle_name_ur')
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="bundle_description_en">Bundle Description (English)</label>
                            <textarea name="bundle_description_en" id="" class="form-control @error('bundle_description_en') is-invalid @enderror" rows="5">{{old('bundle_description_en')!=null? old('bundle_description_en'):$bundles->translate('en')->description}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bundle_description_ur">Bundle Description (اردو)</label>
                            <textarea name="bundle_description_ur" id="" class="form-control @error('bundle_description_ur') is-invalid @enderror" rows="5">{{old('bundle_description_ur')!=null? old('bundle_description_ur'):$bundles->translate('ur')->description}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="discount_percent">Discount Percentage</label>
                            <input type="number" name="discount_percent" class="form-control @error('discount_percent') is-invalid @enderror" value="{{old('discount_percent')!=null? old('discount_percent'):$bundles->discount_percent}}" id="discount_percent">
                            @error('discount_percent')
                                <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-lg-8">
                                    <label for="bundle_image">Bundle Image</label>
                                    <input type="file" accept="image/*" class="form-control-file @error('bundle_image') is-invalid @enderror" name="bundle_image" id="bundle_image">
                                    @error('bundle_image')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <img src="{{$bundles->image_path}}" height="80" width="auto" alt="" id="bundleImage" srcset="" class="float-right mr-5">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <div class="form-check d-inline-block">
                                <input type="checkbox" class="form-check-input" name="active" id="active"  value="{{old('active')!=null? old('active'):$bundles->active}}" {{old('active')==1||$bundles->active==1? 'checked':''}}>
                                <label class="form-check-label" for="active">Active</label>
                            </div>
                            <button type="button" class="btn btn-success btn-sm float-right" id="btnAddProduct">Add Product</button>
                        </div>
                        <div class="col-12">
                            <table class="table table-bordered" id="bundleDetailTable">
                                <thead>
                                    <tr>
                                        <th>Select Product</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (old('product') != null)
                                            @foreach (old('product') as $key => $oldValues)
                                            <input type="hidden" name="bundle_detail_id[]" value="{{old('bundle_detail_id.'.$key)}}">
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="product[]" id="" class="form-control select2 @error('product.'.$key) is-invalid @enderror">
                                                            <option value>Select Product</option>
                                                            @foreach ($products as $item)
                                                                <option value="{{$item->id}}" {{old('product.'.$key) == $item->id?'selected':''}}>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('product.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control @error('quantity.'.$key) is-invalid @enderror" name="quantity[]" placeholder="Enter Quantity" value="{{old('quantity.'.$key)}}">
                                                        @error('quantity.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    
                                                </td>
                                                <td>
                                                    @if ($key != 0)
                                                    <button class="btn btn-danger btn-sm btnRowDelete" data-id="{{old('bundle_detail_id.'.$key)}}"><i class="fa fa-trash"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @elseif ($bundles->bundleDetails != null)
                                            @foreach ($bundles->bundleDetails as $val)
                                            <input type="hidden" name="bundle_detail_id[]" value="{{$val->id}}">
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="product[]" id="" class="form-control select2">
                                                            <option value>Select Product</option>
                                                            @foreach ($products as $item)
                                                                <option value="{{$item->id}}" {{$val->product_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" name="quantity[]" placeholder="Enter Quantity" value="{{$val->quantity}}">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger btn-sm btnRowDelete" data-id="{{$val->id}}"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach        
                                    @else
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <select name="product[]" id="" class="form-control select2">
                                                        <option value>Select Product</option>
                                                        @foreach ($products as $item)
                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="number" class="form-control" name="quantity[]" placeholder="Enter Quantity">
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('bundles.index')}}" class="btn btn-default">Back</a>
                  </div>
                </form>
              </div>
            
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/html" id="data-row">
    <tr>
        <td>
            <div class="form-group">
                <select name="product[]" id="" class="form-control select2detail">
                    <option value>Select Product</option>
                    @foreach ($products as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </td>
        <td>
            <div class="form-group">
                <input type="number" class="form-control" name="quantity[]" placeholder="Enter Quantity">
            </div>
        </td>
        <td>
            <button class="btn btn-danger btn-sm btnRowDelete" data-id="-1"><i class="fa fa-trash"></i></button>
        </td>
    </script>
@endsection
@push('custom-script')
<script src="{{asset('backend/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js" integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g==" crossorigin="anonymous"></script>

    <script>
        var fileInput = document.querySelector("#bundle_image")
        var ImageElement = document.querySelector("#bundleImage")
        
        fileInput.addEventListener("change",function(){
            if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                ImageElement.setAttribute("src", e.target.result);
            }
            reader.readAsDataURL(fileInput.files[0]); // convert to base64 string
            }
        })
        jQuery(document).ready(function(){
            $('.select2').select2({
                theme: 'bootstrap4'
            })
        })
        $(document).on('click','#btnAddProduct',function(){
            $('#bundleDetailTable').find('tbody').append($('#data-row').html());
            $('.select2detail').select2({
                theme: 'bootstrap4'
            })
        });
        $(document).on('click','.btnRowDelete',function(e){
            e.preventDefault();

            let _this = $(this);
            let id = _this.data('id');

            if(id != -1)
            {
                Swal.fire({
                icon: "warning",
                title:"Do you want to save the changes?",
                showCancelButton: true,
                confirmButtonText: `Ok`,

                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:"{{route('AjaxCallForBundleProductDelete')}}",    
                            type: "POST",    
                            data: {
                                _token: "{{ csrf_token() }}", 
                                id: id
                            },
                            success:function(result){
                                if(result)
                                {
                                    Swal.fire('Saved!', '', 'success')
                                    _this.closest('tr').remove();

                                }
                                else{
                                    Swal.fire('Try Later!', '', 'error')
                                }
                            }
                        });
                    } 
                    else{
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                })

            }
            else{
                $(this).parents('tr').remove();
            }
        });
    </script>
@endpush