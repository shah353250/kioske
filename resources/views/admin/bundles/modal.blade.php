<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label>Bundle Image:</label><br>
            <img src="{{$bundle->image_path}}" height="200" width="auto" alt="" srcset="">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Discount Percentage %:</label>
            <p>{{$bundle->discount_percent}}</p>

            <label>Status:</label>
            <p>{{(($bundle->active==1)? 'Active':'Deactive')}}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label>Name (English):</label>
            <p>{{$bundle->translate('en')->name}}</p>
            <label>Description (English):</label><br>
            <p>{{$bundle->translate('en')->description}}</p>
            
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label>Name (اردو):</label>
            <p>{{$bundle->translate('ur')->name}}</p>

            <label>Description (اردو):</label><br>
            <p>{{$bundle->translate('ur')->description}}</p>
        </div>

        

    </div>
    
</div>

<hr>
<div class="row">
    <div class="form-group col-md-12">
        <h4 style="text-align:center"> Product Detail </h4>
    </div>
</div>

@php

$i = 1;
$amount = 0;

@endphp

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Product</th>
            <th>Quantity</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($bundle->bundleDetails as $val)

        <tr>
            <td>{{$val->product->translate('en')->name}}</td>
            <td>{{$val->quantity}}</td>
        </tr>

        @php
        $amount += ($val->quantity * $val->product->discount_price);
        @endphp

        @endforeach

    </tbody>
</table>

@php
$discount_amount = $amount - ($amount * ($bundle->discount_percent / 100));
@endphp
{{-- // original_price - (original_price * discount / 100) --}}

<div class="row">
    <div class="form-group col-md-12">
        <h4 style="text-align:right"> Total Amount : {{$amount}} </h4>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        <h4 style="text-align:right"> Total Discount Amount : {{round($discount_amount)}} </h4>
    </div>
</div>
