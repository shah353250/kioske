@extends('admin.layouts.app')


@push('custom-css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">


    
@endpush

@section('content')

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3>View all Bundles</h3>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{route('bundles.create')}}" class="btn btn-success" style="float:right" >Add Bundle</a>
                                </div>
                            </div>
                        </div>
                        <div class=" ">
                            
                            <br>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#menu1">Active</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu2">Deactive</a>
                                </li>
                                
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane active"><br>

                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="example1">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Name (English)</th>
                                                    {{-- <th>Description (English)</th> --}}
                                                    <th>Name (اردو)</th>
                                                    {{-- <th>Description (اردو)</th> --}}
                                                    <th>Discount Percentage %</th>
                                                    <th>Image</th>
                                                    <th>Status</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $count = 0;
                                                @endphp
                                                @foreach ($bundles as $item)

                                                @if ($item->active == 1)
                                                <tr>
                                                    <td>{{++$count}}</td>
                                                    <td>{{$item->translate('en')->name}}</td>
                                                    {{-- <td>{{$item->translate('en')->description}}</td> --}}
                                                    <td>{{$item->translate('ur')->name}}</td>
                                                    {{-- <td>{{$item->translate('ur')->description}}</td> --}}
                                                    <td>{{$item->discount_percent}}</td>
                                                    <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset=""></td>
            
                                                    <td>{{$item->active==1? 'Active':'Deactive'}}</td>
                                                    <td>
                                                        <button class="btn btn-warning btn-sm view-bundle" data-id="{{$item->id}}"><i class="fa fa-eye"></i></button>
                                                        <a href="{{route('bundles.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade"><br>
                                    <div class="card-body">
                                        <div id="msg"></div>
                                        <table class="table table-bordered table-striped" id="example2">
                                            <thead>
                                                <tr>
                                                    <th>S. No#</th>
                                                    <th>Name (English)</th>
                                                    {{-- <th>Description (English)</th> --}}
                                                    <th>Name (اردو)</th>
                                                    {{-- <th>Description (اردو)</th> --}}
                                                    <th>Discount Percentage %</th>
                                                    <th>Image</th>
                                                    <th>Status</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $count1 = 0;
                                                @endphp                                    
                                                @foreach ($bundles as $item)
                                                @if ($item->active != 1)
                                                <tr>
                                                    <td>{{++$count1}}</td>
                                                    <td>{{$item->translate('en')->name}}</td>
                                                    {{-- <td>{{$item->translate('en')->description}}</td> --}}
                                                    <td>{{$item->translate('ur')->name}}</td>
                                                    {{-- <td>{{$item->translate('ur')->description}}</td> --}}
                                                    <td>{{$item->discount_percent}}</td>
                                                    <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset=""></td>
            
                                                    <td>{{$item->active==1? 'Active':'Deactive'}}</td>
                                                    <td>
                                                        <button class="btn btn-warning btn-sm view-bundle" data-id="{{$item->id}}"><i class="fa fa-eye"></i></button>
                                                        <a href="{{route('bundles.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                             
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">

                @if (Session::has('message'))
                <div class="col-12 mt-3">
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{session()->get('message')}}
                    </div>
                </div>
                @endif

                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3>View all Bundles</h3>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{route('bundles.create')}}" class="btn btn-success" style="float:right" >Add Bundle</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>S. No#</th>
                                        <th>Name (English)</th>
                                        <th>Name (اردو)</th>
                                        <th>Discount Percentage %</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1
                                    @endphp

                                    @foreach ($bundles as $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$item->translate('en')->name}}</td>
                                        <td>{{$item->translate('ur')->name}}</td>
                                        <td>{{$item->discount_percent}}</td>
                                        <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset=""></td>

                                        <td>{{$item->active==1? 'Active':'Deactive'}}</td>
                                        <td>
                                            <button class="btn btn-warning btn-sm view-bundle" data-id="{{$item->id}}"><i class="fa fa-eye"></i></button>
                                            <a href="{{route('bundles.edit',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>

        
    </section>
</div> --}}

<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Bundle Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

@endsection



@push('custom-script')

    <!-- DataTables  & Plugins -->
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    

    <script>
        $(document).ready(function () {
            $("#example1").DataTable();
            $("#example2").DataTable();



            $('.view-bundle').click(function(){
                let _this   = $(this);
                let id      = _this.data('id');
                $.ajax({
                    url:"{{route('AjaxCallForViewBundle')}}",    
                    type: "POST",    
                    data: {
                        _token: "{{ csrf_token() }}", 
                        id: id
                    },
                    
                    success:function(result){
                        console.log('result',result);
                        if(result)
                        {
                            $('#modal-lg').modal('show');
                            $('#modal-body').html(result);
                        }
                    }
                    
                })
            });
            
        });
    </script>
    
@endpush
