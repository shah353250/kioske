<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{asset('frontend/js/3.2.1/jquery.min.js')}}"></script>
<script src="{{asset('frontend/js/4.1.1/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/js/custom.js')}}"></script>
<link href="{{asset('frontend/css/4.1.1/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<title>Kiosk | Online Supermarket | Terms And Conditions</title>
<link rel="shortcut icon" type="image/jpg" href="{{asset('frontend/img/kiosk-favicon.png')}}">
<style>
  .main-ol { padding-left: 0px; padding-top:30px; list-style-type: none;}
  .main-ul {padding-left: 80px;padding-bottom: 30px; list-style-type:disc;}
  .main-ul li { font-size: 18px; }
  /* .next-li { display: block }
  .next-li:before { content: counters(item, ".") " "; counter-increment: item } */
</style>
</head>
<!------ Include the above in your HEAD tag ---------->
<body>
<!-- Nav Start -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top hide-nav" id="banner">
	<div class="container">
  <!-- Brand -->
  <a class="navbar-brand" href="/">
    <img src="{{asset('frontend/img/logo-2.png')}}" class="main-logo">
    <img src="{{asset('frontend/img/logo-white.png')}}" class="white-bg-logo" style="display: none;"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/about-us">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contact">Contact Us</a>
      </li>
      <li class="nav-item download-app-btn">
        <a class="nav-link" href="#">Launching 1<sup>st</sup> RAMADAN!</a>
      </li> 
    </ul>
  </div>
	</div>
</nav>
<!-- Nav End -->
<div class="banner">
	<div class="container">
    <div class="row">  
      <div class="banner-text col-lg" style="text-align:left;">
        <h3 class="about-sub-heading" style="text-align:center;">Terms and Conditions</h3>
        <p class="about-p">This page tells you the terms and conditions for using Kiosk: Online Supermarket (herein also called “Kiosk”) and ordering Products from the mobile application. You should understand that by ordering any of our listed Products, you agree to be bound by these terms and conditions. Please click “I Agree” at the end of these terms and conditions if you accept them. Please understand that if you refuse to accept these terms and conditions, you will not be able to order any Products from Kiosk: Online Supermarket. By continuing to use our app, you agree to and abide by the terms and conditions given on this page.</p>
        <ol class="main-ol">
            <li class="first-li next-li"><span class="about-sub-2-heading">Information about us</span>
              <ul class="main-ol main-ul">
                <li class="">Kiosk: Online Supermarket is operated by the parent company Ayub & Asad Sons(Pvt.) Ltd. We are registered in Pakistan under NTN number A036870.</li>
                <li class="">Through our Platforms, Kiosk links you to the vendors (“Vendors”) for you to order a variety of goods including miscellaneous food items (hereinafter collectively referred to as "Goods") to be delivered to you. When you place an order for Goods from our Vendors (“Order”), Kiosk acts as an agent on behalf of that Vendor to facilitate, process and conclude the order and subsequently for either us or the Vendor to deliver your Order to you. Vendors may be owned and operated by third party vendors, our affiliate companies, or us.</li>
                <li class="">For customer support, you may reach out to us via email or through our in-app customer support call feature.</li>
              </ul>
            </li>
            <br/>
            
            <li class="first-li next-li"><span class="about-sub-2-heading">Account and Information</span>
              <ul class="main-ol main-ul">
                <li class="">You will need to register for a Kiosk account for you to use the Platform. When you register for a Kiosk account, we will ask you to provide a valid email address, a password and mobile phone number. You can also create an account using your Google account or Facebook account.</li>
                <li class="">Kiosk shall not be liable for Orders that encounter delivery issues due to incomplete, incorrect or missing information provided by you. You are obliged to provide information that is complete, accurate and truthful for the proper processing of the Order, including your delivery address and contact information.</li>
                <li class="">If you wish to delete your Kiosk account, you are required to delete the app itself. We may restrict, suspend or terminate your Kiosk account and/or use of the Platforms, if we reasonably believe that:</li>
                <li class="">someone other than you is using your Kiosk account; or</li>
                <li class="">where you are suspected or discovered to have been involved in any activity or conduct that is in breach of these Terms, our policies and guidelines, or involved in activity or conduct which we deem in our sole discretion to be an abuse of the Platforms</li>
              </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Intellectual Property Rights</span>
              <ul class="about-p main-ul" style="padding-top:30px;">
                <li>All trademarks, logos, images, and service marks, including these Terms as displayed on the Platforms or in our marketing material, whether registered or unregistered, are the intellectual property of Kiosk and/or third parties who have authorized us with the use (collectively the “Trademarks”). You may not use, copy, reproduce, republish, upload, post, transmit, distribute, or modify these Trademarksin any way without our prior express written consent. The use of Kiosk’s trademarkson any other website not approved by us is strictly prohibited. Kiosk will aggressively enforce its intellectual property rights to the fullest extent of the law, including criminal prosecution. Kiosk neither warrants nor represents that your use of materials displayed on the Platforms will not infringe rights of third parties not owned by or affiliated with Kiosk. Use of any materials on the Platforms is at your own risk.
                </li>
              </ul>
            </li>

            <li class="first-li next-li"><span class="about-sub-2-heading">User Representation</span>
              <p class="about-p" style="padding-top:30px;">By using the Service, you represent and warrant that</p>
              <ul class="main-ol main-ul" style="padding-top:0px !important;">
                <li class="">All information you submit will be true, accurate, current, and complete;</li>
                <li class="">you have the legal capacity and you agree to comply with these Terms of Use;</li>
                <li class="">you will not use the Service for any illegal or unauthorized purpose;</li>
                <li class="">your use of the Service will not violate any applicable law or regulation.</li>
              </ul>
            </li>

            <br/>
            <li class="first-li next-li"><span class="about-sub-2-heading">Products</span>
              <ul class="about-p main-ul" style="padding-top:30px;">
                <li>
                  We make every effort to display as accurately as possible the colors, features, specifications, and details of the products available on the App. However, we do not guarantee that the colors, features, specifications, and details of the products will be accurate, complete, reliable, current, or free of other errors, and your electronic display may not accurately reflect the actual colors and details of the products.
                </li>
              
                <li>All products are subject to availability, and we cannot guarantee that items will be in stock. We reserve the right to discontinue any products at any time for any reason. Prices for all products are subject to change</li>
              </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Your status</span>
            <ul class="main-ol main-ul">
              <li class="">By placing an order through our app, you warrant that you are legally capable ofentering into binding contracts.</li>
              <li class="">You do not intend to use our app or service for the sale or delivery of any form of alcohols, drugs, and/or narcotics. We will refuse delivery of any such products to anyone</li>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Orders</span>
            <ul class="main-ol main-ul">
              <li class="">When you place an Order with Kiosk, Kiosk will confirm your order by sending you a confirmation email and a notification through the app.</li>
              <li class="">Minimum Order Value - Kiosk require a minimum order value at this initial stage (“MOV”) before an Order can be placed and delivered to you. Where an applicable Order fails to meet the MOV, you will have the option of paying the difference to meet the MOV or to add more Goods to your Order.</li>
              <li class="">Allergens – Kiosk is not obligated to provide ingredient information or allergen information on the Platforms. Further, Kiosk does not guarantee that the Goods soldby Vendors are free of allergens. If you have allergies, allergic reactions or dietary restrictions and requirements, please contact the Vendor before placing an Order onour Platforms.</li>
              <li class="">Placing the Order</li>
              <ul class="main-ol main-ul">
                <li class="">You are required to enter an address or allow access to your location for Goods to be delivered.</li>
                <li class="">To complete an Order, please follow the onscreen instructions after clicking ‘Add to Cart’. You are required to review and confirm that all the information you provide is true, accurate and complete. An Order is successfully placed when you receive an email confirmation or app notification containing your Order receipt from us.</li>
              </ul>
              <li class="">Cancelling an Order</li>
              <ul class="main-ol main-ul">
                <li class="">We do not entertain cancellation of orders. Once you have placed an Order with Kiosk, it will be delivered to your given address.</li>
              </ul>
              <li class="">Kiosk reserves the right to cancel any Order and/or suspend, deactivate or terminate your Kiosk account in its sole discretion if it reasonably suspects or detects fraudulent behavior or activity associated with your Kiosk account and/or with your Order.</li>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Prices and Payments</span>
            <ul class="main-ol main-ul">
              <li class="">Prices quoted on the Platform shall be displayed in the applicable national currency.</li>
              <li class="">Prices indicated on the Platforms are as at the time of each Order and may be subject to change.</li>
              <li class="">You agree to pay the total sum of cash on delivery when you receive your placed order.</li>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Delivery</span>
            <ul class="main-ol main-ul">
              <li class="">Delivery of Order</li>
              <ul class="main-ol main-ul">
                <li class="">Once an Order has been delivered to your location, you will receive email confirmation and notification on Kiosk app.</li>
              </ul>
              <li class="">Unsuccessful or Failed Deliveries</li>
              <li class="">In cases where we attempt to deliver an Order but we are unable to do so due to the reasons caused by you, including but not limited to:
              <p class="about-p">(i) no one was present or available to receive the Order; or<p> 
              <p class="about-p">(ii) customer was uncontactable despite attempts to reach the customer via the phone number provided; or</p>
              <p class="about-p">(iii) lack of appropriate or sufficient access to deliver the Order successfully; </p>
              <p class="about-p">(iv) lack of a suitable or secure location to leave the Order</p>  
              </li>
              <li class="next-li">No-show Cancellations</li>
              <p class="about-p">If you remain uncontactable or fail to receive the Order within ten (10) minutes fromthe time the Order arrives at your delivery address, Kiosk reserves the right to cancel the Order without refund or remedy to you.</p>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Wrong Order, Missing Items, Defective Goods</span>
            <ul class="main-ol main-ul">
              <li>
                Upon receipt of your Order, if you discover that there are issues with your Order (e.g. wrong order, defective order, or missing items) please contact customer support via one of the methods indicated in Clause 1.3 above immediately. In some cases, Kiosk may request for photographic proof and/or additional information to properly investigate the issue with your Order. If we determine that the Order and/or Goods you received are not of satisfactory condition or quality, we will compensate you for your Order or parts of your Order.
              </li>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Refund</span>
            <ul class="main-ol main-ul">
              <li class="">We do not entertain any refund policy.</li>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Representation, Warranties, Limitation of Liabilities</span>
            <ul class="main-ol main-ul">
              <li class="">Representations and Warranties</li>
              <p class="about-p">You acknowledge and agree that the content on the Platforms is provided on an “as is” and “as available” basis, and that your use of or reliance upon the Platforms andany content, goods, products or services accessed or obtained thereby is at your sole risk and discretion. While Kiosk makes reasonable efforts to ensure the provision of the Platforms and the services we offer, are available at all times, we donot warrant or represent that the Platforms shall be provided in a manner which is secure, timely, uninterrupted, error-free, free of technical difficulties, defects or viruses. Please expect temporary interruptions of the Platform due to scheduled or regular system maintenance work, downtimes attributable to internet or electronic communications or events of force majeure.</p>
              <li class="">Limitation of Liability</li>
              <p class="about-p">To the extent permitted by law, Kiosk (which shall include its employees, directors, agents, representatives, affiliates and parent company) exclude all liability (whetherarising in contract, in negligence or otherwise) for loss or damage which you or any third party may incur in connection with our Platforms, our services, and any website linked to our Platforms and any content or material posted on it. Your exclusive remedy with respect to your use of the Platforms is to discontinue your use of the Platforms. The Kiosk entities, their agents, representatives, and service providers shall not be liable for any indirect, special, incidental, consequential, or exemplary damages arising from your use of the Platforms or for any other claim related in any way to your use of the Platforms.</p>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Mobile Application License</span>
            <ul class="main-ol main-ul">
              <li class="">Use License</li>
              <p class="about-p">If you access this mobile application, then we grant you a revocable, non-exclusive, non-transferable, limited right to install and use the mobile application on wireless electronic devices owned or controlled by you, and to access and use the mobile application on such devices strictly in accordance with the terms and conditions of this mobile application license contained in these Terms of Use.</p>
              <p class="about-p">You shall not:</p>
              <p class="about-p">(1) decompile, reverse engineer, disassemble, attempt to derive the source code of,or decrypt the application;</p>
              <p class="about-p">(2) make any modification, adaptation, improvement, enhancement, translation, or derivative work from the application;</p>
              <p class="about-p">(3) violate any applicable laws, rules, or regulations in connection with your access or use of the application;</p>
              <p class="about-p">(4) remove, alter, or obscure any proprietary notice (including any notice of copyright or trademark) posted by us or the licensors of the application;</p>
              <p class="about-p">(5) use the application for any revenue generating endeavor, commercial enterprise, or other purpose for which it is not designed or intended;</p>
              <p class="about-p">(6) make the application available over a network or other environment permitting access or use by multiple devices or users at the same time;</p>
              <p class="about-p">(7) use the application for creating a product, service, or software that is, directly orindirectly, competitive with or in any way a substitute for the application;</p>
              <p class="about-p">(8) use the application to send automated queries to any website or to send any unsolicited commercial e-mail;</p>
              <p class="about-p">(9) use any proprietary information or any of our interfaces or our other intellectual property in the design, development, manufacture, licensing, or distribution of any applications, accessories, or devices for use with the application.</p>
            </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Personal Information</span>
              <ul class="main-ol main-ul">
                <li>
                You agree and consent to Kiosk and any of its affiliate companies collecting, using, processing and disclosing your Personal Data in accordance with these Terms and as further described in our Privacy Policy. Our Privacy Policy is available via the linkson our Platforms, and shall form a part of these Terms.
                </li>
              </ul>
            </li>
            <br/>

            <li class="first-li next-li"><span class="about-sub-2-heading">Changes to Terms</span>
              <ul class="main-ol main-ul">
                <li>
                  Kiosk may amend these Terms at any time in its sole discretion. The amended Terms shall be effective immediately upon posting and you agree to the new Terms by continued use of the Platforms. It is your responsibility to check the Terms regularly. If you do not agree with the amended Terms, whether in whole or in part, you must stop using the Platforms immediately.
                </li>
              </ul>
            </li>
        </ol>
      </div>
    </div>
	</div>
</div>
<footer>
  <div class="container">
    <div class="footer-main row">
      <div class="col-lg-8 col-md-8">
        <div class="footer-logo">
          <img src="{{asset('frontend/img/logo-2.png')}}">
        </div>
        <div class="foo-logo-below">
          <p>An Online Super Market</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-4">
         <div>
            <h4 class="font-control clr-white">Need help? Reach out to us!</h4>
            <a href="tel:0322-8041107" class="font-control">0322-8041107</a><br/>
            <a href="mailto:kioskgrocer@gmail.com">kioskgrocer@gmail.com</a>
        </div>
        <div class="row row-social-ico">
          <div class="col-lg row-social-top">
            <ul class="foot-menu">
              <li><a href="/privacy-policy" class="clr-white">Privacy</a></li>
              <li><a href="/terms-and-conditions" class="clr-white">Terms of use</a></li>
            </ul>
          </div>
          <div class="col-lg row-social-bot">
            <ul class="foot-menu">
                <li><a target="_blank" href="https://www.instagram.com/kioskonlinesupermarket/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Kiosk-Online-Supermarket-107241198130693" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <!-- <li><a href=""><i class="fa fa-twitter"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <p>© Copyright 2021 Kiosk. All rights reserved.</p>
    </div>
  </div>
</footer>
</body>
</html>