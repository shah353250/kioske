<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{asset('frontend/js/3.2.1/jquery.min.js')}}"></script>
<script src="{{asset('frontend/js/4.1.1/bootstrap.min.js')}}"></script>

<script src="{{asset('frontend/js/custom.js')}}"></script>
<link href="{{asset('frontend/css/4.1.1/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<title>Kiosk | Online Supermarket | Privacy Policy</title>
<link rel="shortcut icon" type="image/jpg" href="{{asset('frontend/img/kiosk-favicon.png')}}">
</head>
<!------ Include the above in your HEAD tag ---------->
<body>
<!-- Nav Start -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top hide-nav" id="banner">
	<div class="container">
  <!-- Brand -->
  <a class="navbar-brand" href="/">
    <img src="{{asset('frontend/img/logo-2.png')}}" class="main-logo">
    <img src="{{asset('frontend/img/logo-white.png')}}" class="white-bg-logo" style="display: none;"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/about-us">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contact">Contact Us</a>
      </li>
      <li class="nav-item download-app-btn">
        <a class="nav-link" href="#">Launching 1<sup>st</sup> RAMADAN!</a>
      </li> 
    </ul>
  </div>
	</div>
</nav>
<!-- Nav End -->
<div class="banner">
	<div class="container">
    <div class="row">  
      <div class="banner-text col-lg" style="text-align:left;">
        <h3 class="about-sub-heading" style="text-align:center;">Privacy Policy</h3>
        <p class="about-p">This page tells you the terms of use and privacy on which you may make use of our mobile app Kiosk: Online Supermarket, whether as a guest or a registered user. Please read these terms of use and privacy carefully before you start to use the service. By continuing to use our service, you agree to abide by them. If you do not agree to these terms of use and privacy, please refrain from using our service.
</p>
        <h6 class="about-sub-2-heading">Collecting Personal Information </h6>
        <p class="about-p">We collect personal information from you when you order goods or services from us. This includes email address, password, contact number or Google ID or Facebook ID. We also collect information when you complete any customer survey. Website usage information may also be collected using cookies.
</p>
        <h6 class="about-sub-2-heading">Use of Information Collected</h6>
        <p class="about-p">Information that we collect from you is used to process your order and to manage your account. We obtain this information or any other personal information (such as your name) for the purpose of delivering our service. Your address, contact number, and name will be transmitted to delivery persons (and/or Kiosk representatives, owners, employees, entities) for the purpose of completing the service only. We do not disclose this information to third-parties or use it for other purposes unless for the purpose of brand’s marketing, which is done anonymously. We will only use personal information to curate our business’s outreach and to make the service better.
</p>
        <h6 class="about-sub-2-heading">Accessing our service</h6>
        <p class="about-p last-p">We will not be liable if for any reason our service is temporarily unavailable at any time or for any period. You are responsible for making all the arrangements necessary for you to have access to our service. You are also responsible for ensuring that all persons who access our service through your internet connection are aware of these terms, and that they comply with them. </p>
      </div>
    </div>
	</div>
</div>
<footer>
  <div class="container">
    <div class="footer-main row">
      <div class="col-lg-8 col-md-8">
        <div class="footer-logo">
          <img src="{{asset('frontend/img/logo-2.png')}}">
        </div>
        <div class="foo-logo-below">
          <p>An Online Super Market</p>
        </div>
      </div>

      <div class="col-lg-4 col-md-4"  id="contact">
         <div>
            <h4 class="font-control clr-white">Need help? Reach out to us!</h4>
            <a href="tel:0322-8041107" class="font-control">0322-8041107</a><br/>
            <a href="mailto:kioskgrocer@gmail.com">kioskgrocer@gmail.com</a>
        </div>
        <div class="row row-social-ico">
          <div class="col-lg row-social-top">
            <ul class="foot-menu">
              <li><a href="/privacy-policy" class="clr-white">Privacy</a></li>
              <li><a href="/terms-and-conditions" class="clr-white">Terms of use</a></li>
              
            </ul>
          </div>
          <div class="col-lg row-social-bot">
            <ul class="foot-menu">
                <li><a target="_blank" href="https://www.instagram.com/kioskonlinesupermarket/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Kiosk-Online-Supermarket-107241198130693" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <!-- <li><a href=""><i class="fa fa-twitter"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <p>© Copyright 2021 Kiosk. All rights reserved.</p>
    </div>
  </div>
  
 
  
</footer>

</body>
</html>
