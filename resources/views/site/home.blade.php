<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{asset('frontend/js/3.2.1/jquery.min.js')}}"></script>
<script src="{{asset('frontend/js/4.1.1/bootstrap.min.js')}}"></script>

<script src="{{asset('frontend/js/custom.js')}}"></script>
<link href="{{asset('frontend/css/4.1.1/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<title>Kiosk | Online Supermarket | Home</title>
<link rel="shortcut icon" type="image/jpg" href="{{asset('frontend/img/kiosk-favicon.png')}}">
</head>
<!------ Include the above in your HEAD tag ---------->
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
	<div class="container">
  <!-- Brand -->
  <a class="navbar-brand" href="/">
    <img src="{{asset('frontend/img/logo-2.png')}}" class="main-logo">
    <img src="{{asset('frontend/img/logo-white.png')}}" class="white-bg-logo" style="display: none;"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/about-us">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contact">Contact Us</a>
      </li>
      <li class="nav-item download-app-btn">
        <a class="nav-link" href="#">Launching 1<sup>st</sup> RAMADAN!</a>
      </li> 
    </ul>
  </div>
	</div>
</nav>

<div class="banner">
	<div class="container">
    <div class="row">  
      <div class="banner-text col-lg">
      <div class="banner-heading">
        <h1>Shop for groceries anytime, anywhere from your online supermarket</h1>
        <p>Start shopping now by downloading the app</p>
      </div>
      <div class="banner-btns">
        <a href="https://play.google.com/store/apps/details?id=com.Kiosk.kioskCustomer" target="_blank"><i class="fa fa-android" aria-hidden="true"></i>Android</a>
      </div>
      </div>
      <div class="col-lg banner-img">
        <img src="{{asset('frontend/img/home_banner_2.png')}}">
      </div>
    </div>
	</div>
</div>
<section id="about">
    <div class="container">
        <div class="text-intro">
          <h1>Grocery delivery you can count on</h1>
          
        </div>
        <div class="row about-3-col">
          <div class="col-sm about-1">
            <h3>Choose what you want</h3>
            <img src="{{asset('frontend/img/pad-in-hand.png')}}">
          </div>
          <div class="col-sm about-2">
            <h3>See real-time up-dates</h3>
            <img src="{{asset('frontend/img/boy-with-phone.png')}}">
          </div>
          <div class="col-sm about-3">
            <h3>Get your items same-day</h3>
            <img src="{{asset('frontend/img/girl-signing.png')}}">
          </div>
        </div>
    </div>
</section>
<section id="call-to-action">
  <div class="container">
    <div class="call-section row">
      <div class="call-img-left md-2">
        <img src="{{asset('frontend/img/logo-dark.png')}}">
      </div>
      <div class="call-to-action-text md-10">
        <h2>Start ordering groceries with Kiosk</h2>
        <a href="https://play.google.com/store/apps/details?id=com.Kiosk.kioskCustomer" target="_blank"><i class="fa fa-android" aria-hidden="true"></i>Android</a>
      </div>
    </div>
  </div>
</section>
<footer>
  <div class="container">
    <div class="footer-main row">
      <div class="col-lg-8 col-md-8">
        <div class="footer-logo">
          <img src="{{asset('frontend/img/logo-2.png')}}">
        </div>
        <div class="foo-logo-below">
          <p>An Online Super Market</p>
        </div>
      </div>

      <div class="col-lg-4 col-md-4"  id="contact">
         <div>
            <h4 class="font-control clr-white">Need help? Reach out to us!</h4>
            <a href="tel:0322-8041107" class="font-control">0322-8041107</a><br/>
            <a href="mailto:kioskgrocer@gmail.com">kioskgrocer@gmail.com</a>
        </div>
        <div class="row row-social-ico">
          <div class="col-lg row-social-top">
            <ul class="foot-menu">
              <li><a href="/privacy-policy" class="clr-white">Privacy</a></li>
              <li><a href="/terms-and-conditions" class="clr-white">Terms of use</a></li>
              
            </ul>
          </div>
          <div class="col-lg row-social-bot">
            <ul class="foot-menu">
                <li><a target="_blank" href="https://www.instagram.com/kioskonlinesupermarket/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Kiosk-Online-Supermarket-107241198130693" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <!-- <li><a href=""><i class="fa fa-twitter"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <p>© Copyright 2021 Kiosk. All rights reserved.</p>
    </div>
  </div>
  
 
  
</footer>

</body>
</html>
