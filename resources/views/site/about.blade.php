<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{asset('frontend/js/3.2.1/jquery.min.js')}}"></script>
<script src="{{asset('frontend/js/4.1.1/bootstrap.min.js')}}"></script>

<script src="{{asset('frontend/js/custom.js')}}"></script>
<link href="{{asset('frontend/css/4.1.1/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<title>Kiosk | Online Supermarket | About Us</title>
<link rel="shortcut icon" type="image/jpg" href="{{asset('frontend/img/kiosk-favicon.png')}}">
</head>
<!------ Include the above in your HEAD tag ---------->
<body>
<!-- Nav Start -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top hide-nav" id="banner">
	<div class="container">
  <!-- Brand -->
  <a class="navbar-brand" href="/">
    <img src="{{asset('frontend/img/logo-2.png')}}" class="main-logo">
    <img src="{{asset('frontend/img/logo-white.png')}}" class="white-bg-logo" style="display: none;"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/about-us">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contact">Contact Us</a>
      </li>
      <li class="nav-item download-app-btn">
        <a class="nav-link" href="#">Launching 1<sup>st</sup> RAMADAN!</a>
      </li> 
    </ul>
  </div>
	</div>
</nav>
<!-- Nav End -->
<div class="banner">
	<div class="container">
    <div class="row">  
      <div class="banner-text col-lg" style="text-align:left;">
        <h3 class="about-sub-heading" style="text-align:center;">About Us</h3>
        <h3 class="about-sub-heading">Our Brand</h3>
        <h6 class="about-sub-2-heading">Kiosk’s Logo – Pegasus that Delivers Faster than Anyone</h6>
        <p class="about-p">The symbolization of Pegasus, the divine winged Greek horse, for the logo of Kiosk reflects the company’s fundamental vision of being the industry’s quickest online delivery service. Whether it’s the puff patties for an evening snack, a bag of Basmati rice, or a tube of toothpaste for oral hygiene, Kiosk is designed to take the online shopping experience to newer heights and promises to deliver fresh-quality items within minutes at your doorstep. </p>
        <h6 class="about-sub-2-heading">Kiosk’s Tagline – All-Inclusive</h6>
        <p class="about-p">Online Supermarket is Kiosk’s simple and straight tagline, designed to create an overarching understanding of delivering all items from a typical supermarket to your entered address. From fresh produce, hand-cut meat, dairy, and baked goods, medicines, to canned and packaged items, Kiosk brings it all. It caters to food items, personal care & hygiene, household consumables, and other categories.</p>
        <h6 class="about-sub-2-heading">Its primary blue color is for safety, health, and trust</h6>
        <p class="about-p">Blue will continue to be Kiosk’s primary color choice, deliberately chosen to represent the brand’s goal of maintaining safety and trust to the customers and prioritizing their health above all. </p>
        <h3 class="about-sub-heading">It’s a mart in your hands</h3>
        <p class="about-p">Kiosk was birthed as an idea during the COVID-19 pandemic when shops were shuttered down, mobilization was restricted, and staying indoors seemed the only way to survive the deadly virus. Cooking/eating food, making money, and life, however, had to continue. The challenge was to find a way that reduced the impact on our lives and yet maintained our health. </p>
        <p class="about-p">Keeping in view the social distancing measures, Kiosk is a universal solution to the wide assortment of products relevant to the current situation and beyond. Behind the brand are two young, full-of-spirits, and inspired individuals who have devised a reliable way of bridging the gap between storekeepers and customers. It operates on another level where it caters to all items you’d ever need – from food to medicines, personal care, hygiene to household & other categories. </p>
        <p class="about-p">Without compromising on your health, Kiosk enables you to carry out online shopping as well as get the last-minute cravings without moving an inch, and without waiting for hours. Stay at home, download Kiosk, and order anything and everything from the nearest stores in your community! </p>
        <h3 class="about-sub-heading">Our Promise</h3>
        <p class="about-p">We believe we must not compromise on quality for convenience, which is why in the seam of the path that we have digitized, Kiosk promises to: </p>
        <ul>
            <li>Prioritize health by ensuring deliveries are keeping up with the hygiene standards</li>
            <li>Deliver the freshest items to your doorstep</li>
            <li>Ensure ultra-fast delivery like none other in the industry</li>
            <li>Facilitate a simple shopping experience – choosing from categorized products, adding to cart, and getting it in minutes; it’s that easy. </li>
            <li>Guarantee satisfaction by delivering an exceptional service that’s better than the one you get in stores. </li>
        </ul>
        <p class="about-p last-p">Plus, our help is always just a call away. Try it now!</p>
      </div>
    </div>
	</div>
</div>
<footer>
  <div class="container">
    <div class="footer-main row">
      <div class="col-lg-8 col-md-8">
        <div class="footer-logo">
          <img src="{{asset('frontend/img/logo-2.png')}}">
        </div>
        <div class="foo-logo-below">
          <p>An Online Super Market</p>
        </div>
      </div>

      <div class="col-lg-4 col-md-4" id="contact">
         <div>
            <h4 class="font-control clr-white">Need help? Reach out to us!</h4>
            <a href="tel:0322-8041107" class="font-control">0322-8041107</a><br/>
            <a href="mailto:kioskgrocer@gmail.com">kioskgrocer@gmail.com</a>
        </div>
        <div class="row row-social-ico">
          <div class="col-lg row-social-top">
            <ul class="foot-menu">
              <li><a href="/privacy-policy" class="clr-white">Privacy</a></li>
              <li><a href="/terms-and-conditions" class="clr-white">Terms of use</a></li>
              
            </ul>
          </div>
          <div class="col-lg row-social-bot">
            <ul class="foot-menu">
                <li><a target="_blank" href="https://www.instagram.com/kioskonlinesupermarket/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Kiosk-Online-Supermarket-107241198130693" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <!-- <li><a href=""><i class="fa fa-twitter"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <p>© Copyright 2021 Kiosk. All rights reserved.</p>
    </div>
  </div>
  
 
  
</footer>

</body>
</html>
