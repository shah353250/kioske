<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/customer')->group(function(){
    Route::post('login',"API\Customer\AuthController@login");
    Route::post('register',"API\Customer\AuthController@register");
    Route::get('categories',"API\Customer\CommonController@getCategories")->middleware('localize');
    Route::get('sub_categories/{categoryId}',"API\Customer\CommonController@getSubCategories")->middleware('localize');
    Route::get('products',"API\Customer\CommonController@getProducts")->middleware('localize');
    Route::get('product/{id}',"API\Customer\CommonController@getProduct")->middleware('localize');
    Route::get('bundles',"API\Customer\CommonController@getBundles")->middleware('localize');
    Route::get('product-search/{search}',"API\Customer\CommonController@productSearch")->middleware('localize');
    Route::post('forgot-password',"API\Customer\AuthController@forgotPassword");
    Route::post('change-password',"API\Customer\AuthController@changePassword");
   
    Route::post('guest-order',"API\Customer\OrderController@guestOrder");

    Route::middleware('auth:api')->group(function(){
        Route::post('edit-profile',"API\Customer\AuthController@editProfile");

        Route::get('addresses',"API\Customer\CommonController@getAddresses");
        Route::post('addresses',"API\Customer\CommonController@addAddress");
        Route::put('addresses',"API\Customer\CommonController@updateAddress");
        Route::delete("addresses/{id}","API\Customer\CommonController@deleteAddress");

        Route::post('place-order',"API\Customer\OrderController@placeOrder");
        Route::get('old-orders',"API\Customer\OrderController@userOrders");
        Route::get('previous-order-products/{order_id}',"API\Customer\OrderController@previousOrderProducts")->middleware('localize');

        Route::post('logout',"API\Customer\AuthController@logout");
        Route::post('reset-password',"API\Customer\AuthController@resetPassword");
    });


});

Route::prefix('/rider')->group(function(){
    Route::post('login',"API\Rider\AuthController@login");

    Route::middleware('auth:api')->group(function(){
        
        Route::get('view-orders',"API\Rider\OrderController@viewOrders");
        Route::get('order-detail/{orderId}',"API\Rider\OrderController@orderDetails");
        Route::post('logout',"API\Rider\AuthController@logout");
        Route::post('update-order',"API\Rider\OrderController@updateOrderStatus");
        
    });
});