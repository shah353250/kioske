<?php

use Illuminate\Support\Facades\Route;
use App\Exports\ProductsExport;
use App\Exports\OrdersExport;
use App\Exports\ProductUnitExport;
use App\Exports\ProductSubCategoryExport;
use Maatwebsite\Excel\Facades\Excel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view("site.home");
});
Route::get('/about-us',function(){
    return view("site.about");
});
Route::get('/privacy-policy',function(){
    return view("site.privacy_policy");
});
Route::get('/terms-and-conditions',function(){
    return view("site.terms_and_conditions");
});

Route::get('/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');

Route::middleware('auth:admin')->group(function () {
    
    Route::get('orders',"Admin\OrderController@index")->name('orders.index');
    Route::get('orders/{id}/{notifyId?}',"Admin\OrderController@detail")->name('orders.detail');
    Route::get('home',"Admin\HomeController@index")->name('home');
    Route::post('AjaxCallForAssignRider', 'Admin\OrderController@AjaxCallForAssignRider')->name('AjaxCallForAssignRider');
    Route::get('ajax-notifications', 'Admin\AjaxController@notificationAlert')->name('AjaxNotificationAlert');

    Route::post('/passwordchange','Admin\AuthController@changePassword')->name('change.password');

    //check if user type is equal to admin or not
    Route::middleware('isadmin')->group(function () {
        
        
        Route::resource('productunit',"Admin\ProductUnitController");
        Route::resource('category',"Admin\ProductCategoryController",['except' => ['destroy', 'show']]);
        Route::resource('products',"Admin\ProductsController",['except' => ['destroy', 'show']]);
        Route::resource('riders', "Admin\RiderController");
        Route::resource('customers', "Admin\CustomerController");
        Route::resource('bundles', "Admin\BundleController");

        Route::resource('sub-category', "Admin\ProductSubCategoryController");
        

        Route::post('AjaxCallForBundleProductDelete','Admin\AjaxController@AjaxCallForBundleProductDelete')->name('AjaxCallForBundleProductDelete');
        Route::post('AjaxCallForViewBundle','Admin\AjaxController@AjaxCallForViewBundle')->name('AjaxCallForViewBundle');
        Route::post('AjaxCallForViewProduct','Admin\AjaxController@AjaxCallForViewProduct')->name('AjaxCallForViewProduct');

        
        Route::post('import/products',"Admin\ProductsController@importProducts")->name('import.products');

        Route::get('export/products',function(){
            $filename = "products list ".date('Y_m_d').".xlsx";
            return Excel::download(new ProductsExport, $filename);
        })->name('export.products');
        
        
        Route::get('export/orders',function(){
            $filename = "orders list ".date('Y_m_d').".xlsx";
            return Excel::download(new OrdersExport, $filename);
        })->name('export.orders');

        Route::get('export/product-unit',function(){
            $filename = "product units ".date('Y_m_d').".xlsx";
            return Excel::download(new ProductUnitExport, $filename);
        })->name('export.product-unit');

        Route::get('export/product-subcategory',function(){
            $filename = "product-subcategory ".date('Y_m_d').".xlsx";
            return Excel::download(new ProductSubCategoryExport, $filename);
        })->name('export.product-subcategory');

        Route::get('remove-product-image/{id}','Admin\AjaxController@RemoveProductImage')->name('RemoveProductImage');
        Route::post('add-product-images','Admin\AjaxController@AddProductImages')->name('AddProductImages');
    });
});

