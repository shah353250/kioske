<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRiderIdOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger("rider_id")->nullable()->after("latitude");
            $table->foreign('rider_id')->references("id")->on("users");
            $table->boolean("is_assign")->default(false)->after("rider_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropforeign(['rider_id']);
            $table->dropColumn('rider_id');
            $table->dropColumn('is_assign');
        });
    }
}
