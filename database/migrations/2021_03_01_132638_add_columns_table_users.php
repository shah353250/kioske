<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable()->after('password');
            $table->mediumText('address')->nullable()->after('phone');
            $table->string('nic')->nullable()->after('address');
            $table->enum('user_type',['customer', 'rider'])->after('nic');
            $table->text('facebook_id')->nullable()->after('user_type');
            $table->text('gmail_id')->nullable()->after('facebook_id');
            $table->text('notification_token')->nullable()->after('gmail_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['phone', 'address', 'user_type','nic','facebook_id','gmail_id']);
        });
    }
}
